Get["FIESTA5.m"];

SetOptions[FIESTA,
    "DebugMemory" -> True,
    "DebugParallel" -> True,
    "SectorSymmetries" -> True,
    "OnlyPrepare" -> True
];

If[Not[TrueQ[ValueQ[MasterIndex]]], MasterIndex = 1];

SetOptions[FIESTA,
    DataPath -> "./examples/pl2/temp/db_" <> ToString[MasterIndex] <> "_"
];

Propagators = {-k1^2, -(k1 + q1)^2, -(k1 + k2 - k4 + q1 + 
    q2)^2, -(k1 + k2 - k4 + q1)^2, -(k2 + k3 + q1 + q2)^2, -(-k2 - 
      k3)^2, -(k1 + k2 + k3 - k4 + q1 + q2)^2, -(-k2 - k3 + k4 - q1 - 
      q2)^2, -(k1 + k2 + q1)^2, -k2^2, -k3^2, -k4^2, -(k1 - 
      k2)^2, -(k1 - k3)^2, -(k1 - k4)^2, -(k2 - k4)^2, -(k2 - 
      q2)^2, -(k3 - q1)^2};

Replacements = {q1^2 -> -1, q2^2 -> 0, q1 q2 -> 1/2};

masters = Get["./examples/pl2/pl2-mastersO.m"];

order = masters[[MasterIndex]][[2]];

result = GenerateAnswer[ExpandResult ->False];

Print["Result obtained is:"];
Print[result];

Print["Known analytical result is:"];


answers = Get["./examples/pl2/pl2-answers.m"];
resultA = Normal[Series[masters[[MasterIndex]][[1]] /. answers, {ep, 0, order}]] // N;
Print[resultA];

vars = DeleteCases[Variables[result], ep];
result0 = result /. (Rule[##,0]&/@vars);
(*Print["Difference is "];
Print[InputForm[result0-resultA]];
*)

diff = result0-resultA;
max = Exponent[resultA, 1/ep];
diff = Expand[diff * ep^max];


difco = CoefficientList[diff, ep];

Print["Coefficients of the difference are:"];
Print[difco];

estimate = CoefficientList[ep^max * (result - result0)/.(Rule[##,1]&/@vars), ep];
Print["Coefficients of the error estimate are:"];
Print[estimate];

