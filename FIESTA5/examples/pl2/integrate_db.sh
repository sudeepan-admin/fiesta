if ([ "$#" -ne 1 ] && [ "$#" -ne 2 ] && [ "$#" -ne 3 ] && [ "$#" -ne 4 ]) then
    echo "Argument with a master integral number required";
else
    if ([ "$#" -ne 1 ]) then
        threads="$2";
    else
        threads=1;
    fi;
    if ([ "$#" -ne 1 ] && [ "$#" -ne 2 ]) then
        maxeval="$3";
    else
        maxeval=50000;
    fi;
    if ([ "$#" -ne 1 ] && [ "$#" -ne 2 ] && [ "$#" -ne 3 ]) then
        integrator="$4";
    else
        integrator="vegasCuba";
    fi;
    datapath="./examples/pl2/temp/db_$1_in"
    ./bin/CIntegratePool --in $datapath --threads $threads -v --IntegratorOption maxeval:$maxeval --complex --printIntegrationCommand --Integrator ${integrator} --separateTerms
fi;

