Get["FIESTA5.m"];

SetOptions[FIESTA,
    "DebugMemory" -> True,
    "DebugParallel" -> True,
    "SectorSymmetries" -> True,
    "OnlyPrepare" -> True
];

If[Not[TrueQ[ValueQ[MasterIndex]]], MasterIndex = 1];

SetOptions[FIESTA,
    DataPath -> "./examples/F1/temp/db_" <> ToString[MasterIndex] <> "_"
];

Propagators = {-k1^2, -(k1 + q1)^2, -(k1 + k2 - k4 + q1 + 
    q2)^2, -(k1 + k2 - k4 + q1)^2, -(k2 + k3 + q1 + q2)^2, -(-k2 - 
      k3)^2, -(k1 + k2 + k3 - k4 + q1 + q2)^2, -(-k2 - k3 + k4 - q1 - 
      q2)^2, -(k1 + k2 + q1)^2, -k2^2, -k3^2, -k4^2, -(k1 - 
      k2)^2, -(k1 - k3)^2, -(k1 - k4)^2, -(k2 - k4)^2, -(k2 - 
      q2)^2, -(k3 - q1)^2};

Replacements = {q1^2 -> -1, q2^2 -> 0, q1 q2 -> 1/2};

masters = Get["./examples/F1/F1-masters.m"];

zeros = Position[masters[[MasterIndex]][[2]], 0];


result = GenerateAnswer[];

Print["Result obtained is:"];
Print[result];

Print["Known analytical result is:"];


answers = Get["./examples/F1/F1-answers.m"];
resultA = Normal[Series[masters[[MasterIndex]] /. answers, {ep, 0, 0}]] // N;
Print[resultA];

vars = DeleteCases[Variables[result], ep];

result0 = result /. (Rule[##,0]&/@vars);

(*Print["Difference is "];
Print[InputForm[result0-resultA]];
*)
difco = Abs/@CoefficientList[result0-resultA, 1/ep];

Print["Coefficients of the difference are:"];
Print[difco];

estimate = CoefficientList[(result - result0)/.(Rule[##,1]&/@vars), 1/ep];
Print["Coefficients of the error estimate are:"];
Print[estimate];

