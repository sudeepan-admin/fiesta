SetOptions[$Output,PageWidth->Infinity];

SetOptions[FIESTA,
    "DebugMemory" -> True,
    "DebugParallel" -> True,
    "SectorSymmetries" -> True,
    "OnlyPrepare" -> True
];

If[Not[TrueQ[ValueQ[MasterIndex]]], MasterIndex = 1];

SetOptions[FIESTA,
    DataPath -> "./examples/F1/temp/db_" <> ToString[MasterIndex] <> "_"
];

Propagators = {
    -k1^2, -(k1 + q1)^2, -(k1 + k2 - k4 + q1 + q2)^2, -(k1 + k2 - k4 + q1)^2, -(k2 + k3 + q1 + q2)^2,
    -(-k2 - k3)^2, -(k1 + k2 + k3 - k4 + q1 + q2)^2, -(-k2 - k3 + k4 - q1 - q2)^2, -(k1 + k2 + q1)^2,
    -k2^2, -k3^2, -k4^2, -(k1 - k2)^2, -(k1 - k3)^2, -(k1 - k4)^2, -(k2 - k4)^2, -(k2 - q2)^2, -(k3 - q1)^2
};

Replacements = {q1^2 -> -1, q2^2 -> 0, q1 q2 -> 1/2};

masters = Get["./examples/F1/F1-masters.m"];

zeros = Position[masters[[MasterIndex]][[2]], 0];

SDEvaluate[UF[{k1, k2, k3, k4}, Delete[Propagators, zeros], Replacements], Delete[masters[[MasterIndex]][[2]], zeros], 0];


