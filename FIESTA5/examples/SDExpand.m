Get["examples/include.m"];
result = SDExpand[{x[1] + x[2] + x[3] + x[4], x[1] x[3] + t x[2] x[4], 1} /.x->u, {1, 1, 1, 1}, 1, 0, ExpandVar -> t, EpVar -> Epsilon, PMVar -> PM, AnalyzeWorstPower -> True, XVar->u];
WriteString[Streams["stderr"],ToString[result,InputForm]];
WriteString[$Output,"\n"];
