Get["FIESTA5.m"];

SetOptions[FIESTA,
    "DebugMemory" -> True,
    "DebugParallel" -> True,
    "SectorSymmetries" -> True,
    "OnlyPrepare" -> True
];

SetOptions[FIESTA,
    DataPath -> "./examples/lin2/temp/db_"
];

result = GenerateAnswer[ExpandResult ->False];

Print["Result obtained is:"];
Print[result];

Print["Known analytical result is:"];


resultA = Normal[Series[-(2*Pi*Gamma[-ep]^3*Gamma[1 + 2*ep])/(3*Gamma[-3*ep])*
    Exp[2*ep*EulerGamma], {ep, 0, 0}]] // N;
Print[resultA];

vars = DeleteCases[Variables[result], ep];
result0 = result /. (Rule[##,0]&/@vars);
(*Print["Difference is "];
Print[InputForm[result0-resultA]];
*)

diff = result0-resultA;
max = Exponent[resultA, 1/ep];
diff = Expand[diff * ep^max];


difco = CoefficientList[diff, ep];

Print["Coefficients of the difference are:"];
Print[difco];
Print[Abs/@difco];
estimate = CoefficientList[ep^max * (result - result0)/.(Rule[##,1]&/@vars), ep];
Print["Coefficients of the error estimate are:"];
Print[estimate];

