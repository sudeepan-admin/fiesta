SetOptions[$Output,PageWidth->Infinity];

SetOptions[FIESTA,
    "DebugMemory" -> True,
    "DebugParallel" -> True,
    "SectorSymmetries" -> True,
    "ComplexMode" -> True,
    "SeparateTerms" -> True,
    "OnlyPrepare" -> True
];

SetOptions[FIESTA,
    DataPath -> "./examples/lin2/temp/db" <> "_"
];

SDEvaluate[UF[{l1, l2}, {-l1 u, l2 u, -(l1 + l2 - q)^2, -(l1 - q)^2, -(l2 - q)^2}, {q^2 -> -1,  u^2 -> -1, u*q -> 0}], {1, 1, 1, 1, 1}, 0, d0 -> 3,
    IntegratorOptions -> {{"maxeval", "5000000"}},
    SectorSplitting -> True,
    ContourShiftCoefficient -> 1/2
];


