/** @file CIntegratePoolCommon.cpp
 *
 *  This file is a part of the FIESTA package
 *
 *  This file contains the implementations of functions for the pool,
 *  a program that takes data from a database and sends it to multiple workers for integration.
 *  This part is common for threads and mpi versions.
 */

#include "CIntegratePoolCommon.h"

#include <getopt.h>

#include "../sourcesCommon/error.h"
#include "../sourcesCommon/util.h"

kyotocabinet::ZLIBCompressor<kyotocabinet::ZLIB::Mode::GZIP> databaseCompressor; /**<Compressor initialization for kyoto cabinet*/

void Database::Tune(uint64_t bucketCount, kyotocabinet::Compressor *compressor) {
    auto ok =
        db_.tune_buckets(bucketCount) &&
        db_.tune_alignment(8) &&
        db_.tune_defrag(8) &&
        db_.tune_options(kyotocabinet::HashDB::TCOMPRESS) &&
        db_.tune_compressor(compressor);
    if (!ok) {
        throw FiestaException(Format("Tuning failed: %s", db_.error().message()));
    }
}

void Database::Open(const std::string& path, uint32_t mode) {
    if (!db_.open(path, mode)) {
        throw FiestaException(Format("Failed to open database %s: %s",
            path.c_str(), db_.error().message()));
    }
    open_ = true;
}

std::string Database::Get(const std::string& key) {
    std::string value;
    Get(key, &value);
    return value;
}

void Database::Get(const std::string& key, std::string* value) {
    if (!db_.get(key, value)) {
        throw FiestaException(Format("Get(%s) from database %s failed: %s",
            key.c_str(), db_.path().c_str(), db_.error().message()));
    }
}

bool Database::TryGet(const std::string& key, std::string* value) {
    return db_.get(key, value);
}

void Database::Set(const std::string& key, const std::string& value) {
    if (!db_.set(key, value)) {
        throw FiestaException(Format("Set(%s) to database %s failed: %s",
            key.c_str(), db_.path().c_str(), db_.error().message()));
    }
}

bool Database::Add(const std::string &key, const std::string &value) {
    auto added = db_.add(key, value);
    if (!added && db_.error().code() != kyotocabinet::HashDB::Error::DUPREC) {
        throw FiestaException(Format("Add(%s) to database %s failed: %s",
            key.c_str(), db_.path().c_str(), db_.error().message()));
    }
    return added;
}

bool Database::Remove(const std::string &key) {
    return db_.remove(key);
}

void Database::Close() {
    if (!open_) {
        return;
    }
    open_ = false;
    db_.close();
}

Database::~Database() {
    if (open_) {
        Close();
    }
}

pid_t PoolCommon::OpenProgram(FILE** inputPipe, FILE** outputPipe, const std::string& name) {
    int fdin[2], fdout[2];
    pid_t childpid;
    if (pipe(fdin) < 0) {
        perror("pipe");
        return (0);
    }
    if (pipe(fdout) < 0) {
        perror("pipe");
        return (0);
    }

    if ((childpid = fork()) == -1) {
        perror("fork");
        return (0);
    } /*if((childpid = fork()) == -1)*/

    if (childpid == 0) {
        /* Child process closes up input side of pipe */
        close(fdin[1]);
        close(fdout[0]);

        /*reopen stdin:*/
        close(0);
        if (dup(fdin[0]) < 0) {
            perror("dup");
            return (0);
        }
        /*reopen stdout:*/

        close(1);
        if (dup(fdout[1]) < 0) {
            perror("dup");
            return (0);
        }

        execl(name.c_str(), name.c_str(), nullptr);
        exit(0);
    } else { /* Parent process closes up output side of pipe */

        close(fdin[0]);
        close(fdout[1]);

        if ((*inputPipe = fdopen(fdin[1], "w")) == nullptr) {
            kill(childpid, SIGKILL);
            return (0);
        }

        if ((*outputPipe = fdopen(fdout[0], "r")) == nullptr) {
            fclose(*inputPipe);
            kill(childpid, SIGKILL);
            return (0);
        }
    }
    return childpid;
}

/**
 * Convert given parameter to string.
 * Can handle string parameter.
 * @param x value to convert
 * @return resulting string
 */
template <typename T>
std::string ToString(const T& x) {
    return std::to_string(x);
}

/**
 * Convert given parameter to string.
 * Instantiation for string parameter
 * @param x value to convert
 * @return resulting string
 */
template <>
std::string ToString(const std::string& x) {
    return x;
}

/**
 * Passes given command with parameters to `input` pipe and read `Ok\n` from `output`.
 * Throws exception if anything fails.
 * @param input the input pipe of the other process
 * @param output the output pipe ot the other process
 * @param command command to execute
 * @param args arguments to the command
 * @returns void
 */
template <typename ...Ts>
void Execute(FILE* input, FILE* output, const std::string& command, Ts&& ...args) {
    std::stringstream ss;
    ss << command << '\n';
    auto argStrs = std::vector<std::string>{ToString(args)...};
    for (const auto& argStr : argStrs) {
        ss << argStr << '\n';
    }
    fputs(ss.str().c_str(), input);
    fflush(input);

    auto getDescription = [&] {
        ss.clear();
        ss << command << '(';
        for (auto it = argStrs.begin(); it != argStrs.end(); ++it) {
            ss << *it;
            if (std::next(it) != argStrs.end()) {
                ss << ',';
            }
        }
        ss << ')';
        return ss.str();
    };

    static constexpr int Size = 256;
    char res[Size];
    auto response = fgets(res, 256, output);
    if (response == nullptr) {
        throw FiestaException(Format("Failed to get response for %s from integrator", getDescription().c_str()));
    }
    if (std::string(res) != "Ok\n") {
        throw FiestaException(Format("Failed to execute %s, got error: %s", getDescription().c_str(), res));
    }
}

void PoolCommon::InitCIntegrateParameters(FILE* inputPipe, FILE* outputPipe) {
    Execute(inputPipe, outputPipe, "SetIntegrator", integrator);
    for (auto const& [key, value] : intpar) {
        Execute(inputPipe, outputPipe, "SetCurrentIntegratorParameter", key, value);
    }

    if (mathBinary != "") {
        Execute(inputPipe, outputPipe, "SetMath", mathBinary);
    }
    if (MPMin != "default") {
        Execute(inputPipe, outputPipe, "SetMPMin", MPMin);
    }
    if (MPPrecisionShift != "default") {
        Execute(inputPipe, outputPipe, "SetMPPrecisionShift", MPPrecisionShift);
    }
    if (MPThreshold != "default") {
        Execute(inputPipe, outputPipe, "SetMPThreshold", MPThreshold);
    }
    if (MPSmallX != "default") {
        Execute(inputPipe, outputPipe, "SetMPSmallX", MPSmallX);
    }

    if (PoolCommon::mpfr) {
        Execute(inputPipe, outputPipe, "MPFR");
    }

    if (PoolCommon::native) {
        Execute(inputPipe, outputPipe, "Native");
    }

    if (PoolCommon::noOptimization) {
        Execute(inputPipe, outputPipe, "NoOptimization");
    }

    if (PoolCommon::gpuMode) {
        if (GPUMemoryPart != 1) {
            Execute(inputPipe, outputPipe, "GPUMemoryPart", GPUMemoryPart);
        }
        Execute(inputPipe, outputPipe, "GPUCore", GPUCore);
    }

    if (testF) {
        Execute(inputPipe, outputPipe, "TestF");
    }
    if (NoAVX) {
        Execute(inputPipe, outputPipe, "NoAVX");
    }
}

/**
 * Prints help on program usage
 * @param longOptions The struct containing options of the program
 */
void ShowHelp(const option* longOptions) {
    std::map<std::string, std::string> expl;
    expl.emplace("help", "Show this help.");
    expl.emplace("in", "Obligatory option. Specifies input database. If name does not end with .kch, the extension is appended automatically. "
                "\nIf name ends with in.kch, automatically sets the output database with the same value but ending with out.kch in case it was not set.");
    expl.emplace("out", "Specifies output database. If name does not end with .kch, the extension is appended automatically. "
                "\nObligatory if in name does not end with in or in.kch");
    expl.emplace("bucket", "Specify kyotocabinet bucket value, might be important for speed for databases with a large number of entries.");
    expl.emplace("threads", "The number of CIntegrate threads launched by the pool; ignored by the MPI version.");
    expl.emplace("precision", "Precision in digits printed by the pool, 6 by default.");
    expl.emplace("cIntegratePath", "Provides a path to the CIntegrate binary. \nOverrides --gpu and --complex option used for detection.");
    expl.emplace("expandVar", "Provides a name for the expansion variable, only for printing.");
    expl.emplace("debug", "Prints additional debugging information.");
    expl.emplace("gpu", "Makes the pool use the GPU binary for all (or some) threads.");
    expl.emplace("complex", "For complex integration, uses the corresponding binaries.");
    expl.emplace("gpuThreadsPerNode", "Only if --gpu set. \nMakes only <value> instances per node use GPU.");
    expl.emplace("gpuPerNode", "Only if --gpu set. \nMakes instances using GPU use different GPUs (by default all use GPU number 0).");
    expl.emplace("fromMathematica", "Makes pool write intermediate results in special files for communication with Mathematica.");
    expl.emplace("separateTerms", "Each sector might contain several expressions due to singularity resolution."
                "\nThis option makes terms inside a sector to be sent for integration separately, not mixed.");
    expl.emplace("queueSize", "Sets the queue size of jobs. Default is 32. Large sizes might be needed in case of many threads or MPI so that the queue does not suddenly get empty");
    expl.emplace("printIntegrationCommand", "Prints the command the pool was called with to stdout before working.");
    expl.emplace("Preparse", "Runs the integrators first with a parse check only then with real integration.");
    expl.emplace("prefix", "Sets the prefix for integrals to be integrated from the input database. Prefixes are integration order of form 0-{0, 0}."
                "\nPrefixes for a given task can be obtained with kchashmgr get in.kch N-ForEvaluationString, where N is a task number.");
    expl.emplace("task", "Sets the task integrals to be integrated from the input database. Tasks can be obtained with kchashmgr get in.kch 0-"
                "\nTasks are numbered from 1. For evaluation (no expansion) requests there is only one task normally.");
    expl.emplace("onlyPrepare", "With this option pool only creates the output database and populates it with dummy result entries."
                "\nThis option might be usefull to create a database first and fill in with separate task/prefix/part jobs later.");
    expl.emplace("part", "With <value>=<part number>/<parts number> run this option after preparing the out database consequently "
                "\nfor <part number> = 1 .. <parts number> to get the final result in the out database.");
    expl.emplace("continue", "Makes it possible to continue interrupted evaluation; stores all intermediate results in out database.");
    expl.emplace("continueKeep", "In adition keeps intermediate results in out database after calculation is over.");
    expl.emplace("noOutputDatabase", "Prevents pool from saving results to an output database, only keeps printing them.");
    expl.emplace("printStatistics", "Print average number of sampling points used for each prefix.");
    expl.emplace("balanceSamplingPoints", "First run an estimation run with a small number of sampling points, only then the final run.");
    expl.emplace("balanceMode", "Set mode used for point balancing, what is measured.");
    expl.emplace("balancePower", "Power of fraction which is measured");
    expl.emplace("Integrator", "Sets the integrator provided by one of the integration libraries.");
    expl.emplace("IntegratorOption", "Sets one of the integrator parameters. \nThe syntax for <value> is <option_name>:<option_value>.");
    expl.emplace("Math", "Provides path to the math binary for the integrators. Makes evaluation of non-predefined constants possible.");
    expl.emplace("Test", "Only runs a sample integration test, no real integration is perfomed.");
    expl.emplace("FTest", "Instead of the integration picks the F-function from the database and puts integrators in testF mode."
                "\nIn this mode the functions are also integrated (however the result of F-integration is meaningless)"
                "\nHowever the main purpose is a test that F functions always have same sign (needed in complex mode)."
                "\nIn case they are not, an x value is printed and an error is produced.");
    expl.emplace("Native", "Forces integrators to use native evaluations only, no MPFR.");
    expl.emplace("NoOptimization", "Switches off triad optimization in CIntegrate");
    expl.emplace("MPFR", "Forces integrators to use MPFR evaluations only, no native arithmetics."
                "\nThe following options are used for fine-tuning the point where CIntegrateMP switches to MPFR.");
    expl.emplace("MPThreshold", "Defines the limit for the worst monomial in MPSmallX test point, so that for smaller products MPFR is turned on."
                "\nDefault value is 1E-9");
    expl.emplace("MPSmallX", "Sets the coordinate value (all x) for the small point where the worst monom is measured, default is 0.001.");
    expl.emplace("MPPrecisionShift", "Sets additional bits for mpfr precision when performing calculations in a point, default is 38.");
    expl.emplace("MPMin", "Sets the limit for the worst monom on where to stop using defaut precision and switch to shifts. Default is 1E-48.");
    expl.emplace("NoAVX", "Switches off AVX optimization for function evaluation, has sense only with --enable-avx");

    printf("Usage: ./bin/(CIntegratePool|CIntegratePoolMPI) [options]\n");
    printf("\tOptions starting with a small letter are used by the pool itself, options starting with a capital letter are passed for integration\n");
    for (auto current_option = longOptions; current_option->name != nullptr; ++current_option) {
        auto expl_itr = expl.find(current_option->name);
        std::string value;
        if (expl_itr != expl.end()) {
            value = expl_itr->second;
        } else {
            value = "NO EXPLANATION YET!";
        }
        printf("\t");
        size_t spaces = 0;
        if (current_option->val > 32) {
            printf("-%c", static_cast<const char>(current_option->val));
            spaces += 2;
            if (current_option->has_arg == required_argument) {
                printf(" <value>");
                spaces += 8;
            }
            printf(", ");
            spaces += 2;
        }
        printf("--%s", current_option->name);
        spaces += 2;
        spaces += strlen(current_option->name);
        if (current_option->has_arg == required_argument) {
            printf(" <value>");
            spaces += 8;
        }
        while (spaces < 32) {
            printf(" ");
            ++spaces;
        }
        printf("\t");
        std::string info{value};
        ReplaceAll(info, "\n","\n\t");
        while (spaces) {
            ReplaceAll(info, "\n","\n ");
            --spaces;
        }
        ReplaceAll(info, "\n","\n\t");
        std::cout << info << std::endl;
    }
}

void PoolCommon::ParseArgcArgv(int argc, char* argv[]) {
    static constexpr int defaultOutputPrecision = 6;
    std::cout.precision(defaultOutputPrecision);

    std::stringstream ss;

    for (int i = 0; i < argc; i++) {
        ss << argv[i] << " ";
    }
    integrationCommand = ss.str();

    // options used is pool start with small letters, options passed to CIntegrate --- with capital
    option longOptions[] = {
        {"help", no_argument, nullptr, 'h'},
        {"in",   required_argument,       nullptr, 'i'},
        {"out",   required_argument,   nullptr, 'o'},
        {"bucket",  required_argument, nullptr,    'b'},
        {"threads",  required_argument, nullptr,    't'},
        {"precision",   required_argument,     nullptr, 'p'},
        {"gpu",   no_argument,     nullptr, 'g'},
        {"complex",   no_argument,     nullptr, 'c'},
        {"cIntegratePath",   required_argument,     nullptr, 18},
        {"expandVar",   required_argument,     nullptr, 'e'},
        {"gpuThreadsPerNode",  required_argument, nullptr,    9},
        {"gpuPerNode",  required_argument, nullptr,    10},
        {"fromMathematica",  no_argument, nullptr,    'f'},
        {"separateTerms",   no_argument,     nullptr, 's'},
        {"queueSize",  required_argument, nullptr,    21},
        {"debug",   no_argument,     nullptr, 'd'},
        {"task",   required_argument,     nullptr, 8},
        {"prefix",   required_argument,     nullptr, 7},
        {"onlyPrepare",  no_argument, nullptr,    14},
        {"part", required_argument, nullptr,    1},
        {"continue",   no_argument,     nullptr, 16},
        {"continueKeep",   no_argument,     nullptr, 17},
        {"noOutputDatabase",   no_argument,     nullptr, 22},
        {"printIntegrationCommand",  no_argument, nullptr,    13},
        {"printStatistics",  no_argument, nullptr,    'v'},
        {"balanceSamplingPoints",  no_argument, nullptr,    23},
        {"balanceMode", required_argument, nullptr,    24},
        {"balancePower", required_argument, nullptr,    25},
        // now coming options that are passed to the CIntegrate
        {"Integrator",  required_argument, nullptr,    'I'},
        {"IntegratorOption",  required_argument, nullptr,    'O'}, // used to be intpar
        {"Math",   required_argument,     nullptr, 'M'},
        {"Test",   no_argument,     nullptr, 'T'},
        {"FTest",  no_argument, nullptr,    'F'},
        {"Preparse",  no_argument, nullptr,    15},
        {"Native",  no_argument, nullptr,    19},
        {"NoOptimization",  no_argument, nullptr,    20},
        {"MPFR",  no_argument, nullptr,    11},
        {"MPThreshold",  required_argument, nullptr,    2},
        {"MPPrecisionShift",  required_argument, nullptr,    4},
        {"MPSmallX",  required_argument, nullptr,    5},
        {"MPMin",  required_argument, nullptr,    6},
        {"NoAVX",  no_argument, nullptr,    26},
        {nullptr, 0,                  nullptr,    0}
    };


    std::stringstream shortOptions;
    for (auto current_option = longOptions; current_option->name != nullptr; ++current_option) {
        if (current_option->val > 32) {
            shortOptions << static_cast<char>(current_option->val);
            if (current_option->has_arg == required_argument) {
                shortOptions << ':';
            }
        }
    }

    int c = 0;
    bool helpRequested = false;
    while ((c = getopt_long(argc, argv, shortOptions.str().c_str(), longOptions, nullptr)) != -1) {
        std::string optionArgument = optarg ? std::string(optarg): "";
        switch (c) {
            case 0:
                break;
            case 'h':
                ShowHelp(longOptions);
                helpRequested = true;
                break;
            case 'i': {// in
                if (optionArgument.find(".kch") == std::string::npos) {
                    inputDatabaseName = optionArgument + ".kch";
                } else {
                    inputDatabaseName = optionArgument;
                }
                size_t sitr = inputDatabaseName.find("in.kch");
                if (sitr != std::string::npos) {
                    outputDatabaseName = inputDatabaseName;
                    outputDatabaseName.erase(sitr);
                    outputDatabaseName += "out.kch";
                }
                break;
            }
            case 'o': // out
                if (optionArgument.find(".kch") == std::string::npos) {
                    outputDatabaseName = optionArgument + ".kch";
                } else {
                    outputDatabaseName = optionArgument;
                }
                break;
            case 'p': // precision
                std::cout.precision(std::stoi(optarg));
                break;
            case 'b': // bucket
                bucket = std::stoi(optarg);
                break;
            case 1: // part
                if (sscanf(optarg, "%d/%d", &sectorPart, &sectorParts) != 2) {
                    throw FiestaException("Incorrect syntax after --sectorPart, fraction %d/%d requied");
                }
                break;
            case 't': // threads
                threadsNumber = std::stoi(optarg);
                break;
            case 7: // currentPrefix
                if (optionArgument.find("{") == std::string::npos)
                    inputPrefix = optionArgument + "-{0, 0}";
                else
                    inputPrefix = optionArgument;
                break;
            case 8: // task
                requestedTask = optionArgument;
                break;
            case 's': // separateTerms, former separate_terms
                separateTerms = true;
                break;
            case 'd': // debug
                debug = true;
                break;
            case 'c': //complex
                complexMode = true;
                break;
            case 'g': //gpu
                gpuMode = true;
                break;
            case 9: // gpuThreadsPerNode, former GPUThreadsPerNode
                GPUThreadsPerNode = std::stoi(optarg);
                break;
            case 10: // gpuPerNode, former GPUPerNode
                GPUPerNode = std::stoi(optarg);
                break;
            case 13: // PrintIntegrationCommand, former PrintIntegrationCommand
                printIntegrationCommand = true;
                break;
            case 'f': // fromMathematica, former from_mathematica
                direct = false;
                break;
            case 21: // queueSize
                queueSize = std::stoi(optarg);
                break;
            case 14: // onlyPrepare, former onlyPrepare
                onlyPrepare = true;
                break;
            case 15: // preparse
                preparse = true;
                break;
            case 18: // cIntegratePath, former CIntegratePath
                if (optarg[0] == '/')
                    binaryPath = optionArgument;
                else {
                    binaryPath = std::string(argv[0]);
                    size_t pos = binaryPath.rfind("/");
                    if (pos != std::string::npos) {
                        binaryPath.erase(pos + 1);
                    }
                    binaryPath += optionArgument;
                }
                break;
            case 'e':
                expandVar = std::string{optarg};
                break;
            case 16: // continue
                resultsMode = SaveMode::current;
                break;
            case 17: // continueKeep, former saveAll
                resultsMode = SaveMode::all;
                break;
            case 22: // noOutputDatabase
                resultsMode = SaveMode::noDatabase;
                break;
            case 23: // balanceSamplingPoints
                balanceSamplingPoints = true;
                break;
            case 24: // balanceMode
                if (optionArgument == "realValue") {
                    balanceMode = BalanceMode::realValue;
                } else if (optionArgument == "realError") {
                    balanceMode = BalanceMode::realError;
                } else if (optionArgument == "imaginaryValue") {
                    balanceMode = BalanceMode::imaginaryValue;
                } else if (optionArgument == "imaginaryError") {
                    balanceMode = BalanceMode::imaginaryError;
                } else if (optionArgument == "normValue") {
                    balanceMode = BalanceMode::normValue;
                } else if (optionArgument == "normError") {
                    balanceMode = BalanceMode::normError;
                } else {
                    FIESTA_FAIL("Incorrect balancing mode set");
                }
                break;
            case 25: // balancePower
                balancePower = stod(optionArgument);
                break;
            case 'v':
                PoolCommon::printStatistics = true;
                break;
            // here go options passed to CIntegrate
            case 'I': // Integrator
                integrator = optionArgument;
                break;
            case 'O': {// IntegratorOption, previously intpar, syntax changed to have one argument
                const char* spos = optarg;
                while ((*spos != '\0') && (*spos != ':')) ++spos;
                if (*spos != ':') {
                    throw FiestaException("Incorrect syntax after -O, parameter and value %s:%s requied");
                }
                intpar.emplace(std::string(optarg, spos - optarg), std::string(spos + 1));
                break;
                }
            case 2: // MPThreshold
                MPThreshold = optionArgument;
                break;
            case 4: // MPPrecisionShift
                MPPrecisionShift = optionArgument;
                break;
            case 5: // MPSmallX
                MPSmallX = optionArgument;
                break;
            case 6: // MPMin
                MPMin = optionArgument;
                break;
            case 'T': // test
                testMode = true;
                break;
            case 11: // MPFR, former mpfr
                mpfr = true;
                break;
            case 19: // Native
                native = true;
                break;
            case 20: // NoOptimization
                PoolCommon::noOptimization = true;
                break;
            case 'M': // Math
                mathBinary = optionArgument;
                break;
            case 'F': // FTest, former testF
                testF = true;
                break;
            case 26: // NoAVX
                NoAVX = true;
                break;
        }
    }

    intpar.emplace("maxeval", "50000"); // default value if none is provided by options

    if (helpRequested) {
        return;
    }

    if ((GPUPerNode == 0) || (GPUThreadsPerNode == 0)) {
        gpuMode = false;
    }

    if (binaryPath == "") {
        // setting proper binary path based on pool location and options
        binaryPath = std::string(argv[0]);
        size_t pos = 0;
        while (true) {
            size_t newPos = binaryPath.find("/", pos + 1);
            if (newPos == std::string::npos) {
                break;
            }
            pos = newPos;
        }
        if (pos != 0) {
            binaryPath.erase(pos + 1);
        }
        binaryPath += "CIntegrateMP";
        if (complexMode) {
            binaryPath += "C";
        }
        if (gpuMode) {
            binaryPath += "G";
        }
    }

    if (threadsNumber <= 0) {
        throw FiestaException("Number of threads should be positive");
    }

    if (testMode) {
        inputDatabaseName = "!";
        return;
    }

    if (inputDatabaseName == "") {
        throw FiestaException("Missing input database");
    }

    if (outputDatabaseName == "") {
        throw FiestaException("Missing output database");
    }
}

void PoolCommon::OpenDatabases() {
    inputDatabase.Tune(1llu << bucket, &databaseCompressor);

    inputDatabase.Open(inputDatabaseName, kyotocabinet::HashDB::OREADER | kyotocabinet::HashDB::ONOLOCK);

    if (requestedTask != "all") {  // specific task request
        tasks.push_back(requestedTask);
    } else {
        auto value = inputDatabase.Get("0-");
        value.erase(0, 1);
        value.erase(value.size() - 1);
        size_t pos = 0; // to skip bracket
        while ((pos = value.find(", ")) != std::string::npos) {
            tasks.emplace_back(value.substr(0, pos));
            value.erase(0, pos + 2);
        }
        tasks.push_back(value);
    }

    if (direct && (resultsMode != SaveMode::noDatabase)) {
        outputDatabase.Tune(1llu << bucket, &databaseCompressor);
        int mode = kyotocabinet::HashDB::OWRITER | kyotocabinet::HashDB::OCREATE;
        if ((resultsMode == SaveMode::none) && (sectorParts == 1)) {
            // we do not need output database
            mode |= kyotocabinet::HashDB::OTRUNCATE;
        } else {
            // we need to save all results, do we?
            //mode |= kyotocabinet::HashDB::OAUTOSYNC;
        }
        outputDatabase.Open(outputDatabaseName, mode);

        outputDatabase.Set("0-IntegrationCommand", integrationCommand);

        if (requestedTask != "all") {
            outputDatabase.Set("0-", "{" + requestedTask + "}");
        } else {
            std::string value;
            if (inputDatabase.TryGet("0-", &value)) {
                outputDatabase.Set("0-", value);
            }
        }

        std::string value;
        if (inputDatabase.TryGet("0-RegVar", &value)) {
            outputDatabase.Set("0-RegVar", value);
        }
        if (inputDatabase.TryGet("0-RequestedOrders", &value)) {
            outputDatabase.Set("0-RequestedOrders", value);
        }
    }
}

void PoolCommon::PrepareTaskIntegration() {
    prefixes.clear();
    prefixesSmallVariable.clear();
    results.clear();
    resultsImaginary.clear();

    if (inputPrefix == "") {
        auto value = inputDatabase.Get(currentTask + "-ForEvaluationString");
        size_t pos = 0;
        while ((pos = value.find("|")) != std::string::npos) {
            prefixes.emplace_back(value.substr(0, pos));
            value.erase(0, pos + 1);
        }
    } else {
        prefixes.push_back(inputPrefix);
    }

    if (direct && (resultsMode != SaveMode::noDatabase)) {
        std::vector<std::string> toCopy = {"ForEvaluation", "runorder", "SHIFT", "EXTERNAL", "ExternalExponent"};
        for (const auto& member : toCopy) {
            auto value = inputDatabase.Get(currentTask + "-" + member);
            outputDatabase.Set(currentTask + "-" + member, value);
        }
    }

    auto value = inputDatabase.Get(currentTask + "-" + "SCounter");
    lastSector = std::stoi(value);

    if (onlyPrepare) {
        lastSector = 0;
    } else if (sectorParts != 1) {
        // integrating a sectorPart of terms
        firstSector = (sectorPart - 1) * (lastSector / sectorParts) + 1;
        if (sectorParts != sectorPart) {
            lastSector = (sectorPart) * (lastSector / sectorParts);
        }
    }
}

void PoolCommon::CloseDatabases() {
    inputDatabase.Close();
    if (resultsMode != SaveMode::noDatabase) {
        outputDatabase.Close();
    }
}

std::string PoolCommon::GenerateIntegrand(int sector, int function) {
    std::stringstream ss;

    if (function < 0) {
        int sectorFunctions = -function;

        ss << currentNumberOfVariables << ";\n" << sectorFunctions << ";\n";

        for (int k = 1; k <= sectorFunctions; k++) {
            std::stringstream keyStream;
            keyStream << currentTask << "-" << currentPrefix << "-" << sector << "-" << k << (testF ? "F" : "");
            std::string key = keyStream.str();
            auto value = inputDatabase.Get(key);
            ss << value << "\n";
        }
        for (int k = 1; k <= sectorFunctions; k++) {
            ss << "f[" << k << "]+";
        }
        ss << "0;\n|\n";
    } else {  // separate term
        std::stringstream keyStream;
        keyStream << currentTask << "-" << currentPrefix << "-" << sector << "-" << function << "-" << "N";
        std::string key = keyStream.str();
        auto value = inputDatabase.Get(key);
        ss << value << ";\n" << "0" << ";\n";
        keyStream.str({});
        keyStream << currentTask << "-" << currentPrefix << "-" << sector << "-" << function << (testF ? "F" : "");
        key = keyStream.str();

        inputDatabase.Get(key, &value);
        if (value[0] == '$') {
            // that is an optimized std::string from Mathematica, we do not need to send the dollar sign and the zero number of functions
            value.erase(0, 1);
        }
        ss << value << "\n|\n";
    }
    return ss.str();
}

void PoolCommon::Submitter() {
    try {
        DoSubmit();
    } catch (const std::exception& exception) {
        FIESTA_FAIL(std::string("Submitter failed: ") + exception.what());
        // submitterError = std::current_exception();
    }
}

void PoolCommon::DoSubmit() {
    // in continue mode we do not restart prefix integration
    if ((resultsMode != SaveMode::none)  && (resultsMode != SaveMode::noDatabase)) {
        std::string value;
        if (outputDatabase.TryGet(currentTask + "-" + currentPrefix + "-" + "R", &value)) {
            if (value.substr(0, 6) != "{Print") {
                return;
            }
        }
    }

#ifdef THREADSMODE
    if (currentOnlyEstimate || balanceSamplingPoints) {
        unsigned long long maxeval = std::stoll(intpar["maxeval"]);
        if (currentOnlyEstimate) {
            maxeval /= estimationPart;
        }
        std::string maxevalString = std::to_string(maxeval);
        for (int i = 0; i != threadsNumber; ++i) {
            Execute(pipeToCIntegrate[i], pipeFromCIntegrate[i], "SetCurrentIntegratorParameter", std::string("maxeval"), maxevalString);
        }
    }
#endif

    for (int i = firstSector; i <= lastSector; i++) {
        if (stopSubmitter.load()) {
            return;
        }

        std::string key = currentTask + "-" + currentPrefix + "-" + std::to_string(i);
        auto value = inputDatabase.Get(key);

        auto sectorFunctions = std::stoi(value);

        if ((sectorFunctions == 0) && (!separateTerms)) {
        } else if ((currentNumberOfVariables == "0") && (!separateTerms)) {
            // we are evaluating the result here!
            float result = 0;
            for (int j = 1; j <= sectorFunctions; j++) {
                std::stringstream keyStream;
                keyStream << currentTask << "-" << currentPrefix << "-" << i << "-" << j << (testF ? "F" : "");
                key = keyStream.str();
                inputDatabase.Get(key, &value);
                float res;
                if (sscanf(value.c_str(), "%f", &res) != 1) {
                    throw FiestaException(Format(
                        "Integration string with no variables cannot be transformed to number\n%s\n", value.c_str()));
                }
                result += res;
            }
            std::unique_lock rlock(PoolCommon::rqueueMutex);
            PoolCommon::rjobQueueFilled.wait(rlock, []() { return (PoolCommon::rjobQueue.size() < PoolCommon::queueSize); });
            PoolCommon::rjobQueue.emplace(i, 0, "{" + std::to_string(result) + ",0,0,0}");
            PoolCommon::rjob.notify_one();
        } else if (separateTerms) { // forming a std::string for integration
            for (int j = 1; j <= sectorFunctions; j++) {
                std::stringstream keyStream;
                keyStream << currentTask << "-" << currentPrefix << "-" << i << "-" << j << (testF ? "F" : "");
                key = keyStream.str();
                if ((resultsMode != SaveMode::none) && (resultsMode != SaveMode::noDatabase) && outputDatabase.TryGet(key, &value)) {
                    // we have a result in out
                    std::unique_lock rlock(PoolCommon::rqueueMutex);
                    PoolCommon::rjobQueueFilled.wait(rlock, []() { return (PoolCommon::rjobQueue.size() < PoolCommon::queueSize); });
                    PoolCommon::rjobQueue.emplace(i, j, value);
                    PoolCommon::rjob.notify_one();
                } else {
                    keyStream.str({});
                    keyStream << currentTask << "-" << currentPrefix << "-" << i << "-" << j << "-" << "N";
                    key = keyStream.str();
                    auto localVarsNumber = inputDatabase.Get(key);
                    if ((localVarsNumber == "0") || (localVarsNumber == "-Infinity")) { // no variables, we are evaluating the result here
                        key = currentTask + "-" + currentPrefix + "-" + std::to_string(i) + "-" + std::to_string(j) + (testF ? "F" : "");
                        inputDatabase.Get(key, &value);
                        float res;
                        if (sscanf(value.c_str(), "%f", &res) != 1) {
                            throw FiestaException(Format(
                                "Integration string with no variables cannot be transformed to number\n%s\n",
                                value.c_str()));
                        }
                        std::unique_lock rlock(PoolCommon::rqueueMutex);
                        PoolCommon::rjobQueueFilled.wait(rlock, []() { return (PoolCommon::rjobQueue.size() < PoolCommon::queueSize); });
                        PoolCommon::rjobQueue.emplace(i, j, "{" + std::to_string(res) + ",0,0,0}");
                        PoolCommon::rjob.notify_one();
                    } else {
                        std::unique_lock lock(queueMutex);
                        jobQueueFilled.wait(lock, []() { return (jobQueue.size() < PoolCommon::queueSize); });
                        jobQueue.emplace(i, j, GenerateIntegrand(i, j));
                        job.notify_one();
                    }
                }
            }
        } else {
            std::stringstream keyStream;
            keyStream << currentTask << "-" << currentPrefix << "-" << i << "-" << "0" << (testF ? "F" : "");
            key = keyStream.str();
            if ((resultsMode != SaveMode::none) && (resultsMode != SaveMode::noDatabase) && (outputDatabase.TryGet(key, &value))) {
                // we have a result in out
                std::unique_lock rlock(PoolCommon::rqueueMutex);
                PoolCommon::rjobQueueFilled.wait(rlock, []() { return (PoolCommon::rjobQueue.size() < PoolCommon::queueSize); });
                PoolCommon::rjobQueue.emplace(i, -sectorFunctions, value);
                PoolCommon::rjob.notify_one();
            } else {
                std::unique_lock lock(queueMutex);
                jobQueueFilled.wait(lock, []() { return (jobQueue.size() < PoolCommon::queueSize); });
                jobQueue.emplace(i, -sectorFunctions, GenerateIntegrand(i, -sectorFunctions));
                job.notify_one();
            }
        }
    }
}

void PoolCommon::PrintToolCommand(int sector, int function, const std::string& res) {
    std::cout << "Integration error. To see the problematic integrand, run " << std::endl;
    std::stringstream ss;
    ss << "bin/CIntegrateTool --in " << PoolCommon::inputDatabaseName << " --task " << currentTask << " --prefix \"" << currentPrefix << "\" --sector " << sector;
    if (function <= 0) {
        ss << " --all";
    } else {
        ss << " --function " << function;
    }
    ss << " --Integrator " << integrator;
    for (const auto& parameter : intpar) {
        ss << " --IntegratorOption " << parameter.first << ":" << parameter.second;
    }
    if (mpfr) {
        ss << " --MPFR";
    }
    if (native) {
        ss << " --Native";
    }
    if (PoolCommon::noOptimization) {
        ss << " --NoOptimization";
    }
    if (mathBinary != "") {
        ss << " --Math " << mathBinary;
    }
    if (MPMin != "default") {
        ss << " --MPMin " << MPMin;
    }
    if (MPPrecisionShift != "default") {
        ss << " --MPPrecisionShift " << MPPrecisionShift;
    }
    if (MPThreshold != "default") {
        ss << " --MPThreshold " << MPThreshold;
    }
    if (MPSmallX != "default") {
        ss << " --MPSmallX " << MPSmallX;
    }
    std::cout << ss.str() << std::endl;
    std::cout << "Result is:" << std::endl;
    std::cout << res << std::endl;
}

void PoolCommon::Receiver() {
    try {
        DoReceive();
    } catch (const std::exception& exception) {
        FIESTA_FAIL(std::string("Receiver failed: ") + exception.what());
        // receiverError = std::current_exception();

        // jobQueueFilled.notify_one();   // to get Submitter out if it was waiting
        // job.notify_one();              // to get Integrate_thread or communicate_with_slaves out of waiting for a new task
        // rjobQueueFilled.notify_all();  // to get Integrate threads or communicating with slaves get out of waiting for a free slot in Receiver queue
    }
}

void PoolCommon::DoReceive() {
    int points[10];
    int point = 0;

    // the prefix has form "n-something" (n can be negative), where n is an integer (order) and something is related to small variable
    int currentOrder = std::stoi(currentPrefix);
    size_t pos = currentPrefix.find("-", 1);
    std::string currentPrefixSmallVariable = currentPrefix.substr(pos + 1);
    prefixesSmallVariable.insert(currentPrefixSmallVariable);

    double res1 = 0;
    double res2 = 0;
    double res1_im = 0;
    double res2_im = 0;

    if (direct && !currentOnlyParse) {
        if (!debug) {
            if (currentOnlyEstimate) {
                std::cout << "Estimating";
            } else {
                std::cout << "Integrating";
            }
            std::cout.flush();
        } else {
            std::cout << "\"{\n";
        }
    }

    // in continue mode we do not restart prefix integration
    bool shouldReceive = true;
    if ((resultsMode != SaveMode::none) && (resultsMode != SaveMode::noDatabase)) {
        std::string key = currentTask + "-" + currentPrefix + "-" + "R";
        std::string value;
        if (outputDatabase.TryGet(key, &value)) {
            if (value.substr(0, 6) != "{Print") {
                shouldReceive = false;
                if (sscanf(value.c_str(), "{%lf,%lf,%lf,%lf}", &res1, &res2, &res1_im, &res2_im) != 4) {
                    throw FiestaException("Failed to analyze old result");
                }
                std::cout << "..we have a result..";
                res2 = res2 * res2;
                res2_im = res2_im * res2_im;
            }
        }
    }

    for (int i = 0; i != 10; i++) {
        points[i] = (firstSector - 1) + ((i + 1) * (lastSector - (firstSector - 1))) / 10;
    }

    auto time_startA = std::chrono::steady_clock::now();

    // get the old result if it exists
    if ((sectorParts != 1) && (sectorPart != 1) && (resultsMode != SaveMode::noDatabase)) {
        std::string key = currentTask + "-" + currentPrefix + "-" + "R";
        std::string value;
        if (outputDatabase.TryGet(key, &value)) {
            // old entry exists
            if (value.substr(0, 6) != "{Print") {
                // the entry is not thw warning with zeros
                if (sscanf(value.c_str(), "{%lf,%lf,%lf,%lf}", &res1, &res2, &res1_im, &res2_im) != 4) {
                    throw FiestaException("Failed to analyze old result");
                }
                if (!debug) {
                    std::cout << "(with old result " << res1 << " +- " << sqrt(res2);
                    if (complexMode && (res1_im != 0)) {
                        std::cout << " + ( " << res1_im << " +- " << sqrt(res2_im) << " ) * I";
                    }
                    std::cout << ")";
                }
                res2 = res2 * res2;
                res2_im = res2_im * res2_im;
            }
        }
    }

    int done = 0;
    for (int i = firstSector; i <= lastSector; i++) {
        if (!shouldReceive) {
            break;
        }
        std::string key = currentTask + "-" + currentPrefix + "-" + std::to_string(i);
        auto value = inputDatabase.Get(key);

        int sectorFunctions = std::stoi(value);

        if ((!separateTerms) && (sectorFunctions != 0)) {
            sectorFunctions = 1;
        }

        for (int j = 1; j <= sectorFunctions; j++) {
            std::unique_lock lock(rqueueMutex);
            rjob.wait(lock, []() { return (!rjobQueue.empty()); });
            auto returned = rjobQueue.front();
            rjobQueue.pop();
            lock.unlock();
            rjobQueueFilled.notify_one();

            double temp1, temp2;
            double temp1_im, temp2_im;

            if (sscanf(returned.data.c_str(), "{%lf,%lf,%lf,%lf}", &temp1, &temp2, &temp1_im, &temp2_im) != 4) {
                PrintToolCommand(returned.sector, returned.function, returned.data);
                throw FiestaException("Failed to parse result");
            }

            if (currentOnlyEstimate) {
                double balanceValue = 0.;
                if (balanceMode == BalanceMode::realValue) {
                    balanceValue = abs(temp1);
                } else if (balanceMode == BalanceMode::realError) {
                    balanceValue = abs(temp2);
                } else if (balanceMode == BalanceMode::imaginaryValue) {
                    balanceValue = abs(temp1_im);
                } else if (balanceMode == BalanceMode::imaginaryError) {
                    balanceValue = abs(temp2_im);
                } else if (balanceMode == BalanceMode::normValue) {
                    balanceValue = sqrt(temp1 * temp1 + temp1_im * temp1_im);
                } else if (balanceMode == BalanceMode::normError) {
                    balanceValue = sqrt(temp2 * temp2 + temp2_im * temp2_im);
                }
                balanceValue = pow(balanceValue, balancePower);
                estimatedIntegrals.emplace(std::pair{returned.sector, returned.function}, balanceValue);
            }

            res1 += temp1;
            res2 += (temp2 * temp2);
            res1_im += temp1_im;
            res2_im += (temp2_im * temp2_im);
            if ((resultsMode != SaveMode::none) && (resultsMode != SaveMode::noDatabase) && !currentOnlyEstimate) {
                key = currentTask + "-" + currentPrefix + "-" + std::to_string(returned.sector) + "-" + (returned.function < 0 ? "0" : std::to_string(returned.function));
                if (resultsMode != SaveMode::noDatabase) {
                    outputDatabase.Add(key, returned.data);
                }
                // here we need to add a value to the number of stored entries
                key = currentTask + "-" + currentPrefix + "-" + "done";
                done++;
                int doneBefore = 0;
                if (outputDatabase.TryGet(key, &value)) {
                    doneBefore = std::stoi(value);
                }
                if (done > doneBefore) {
                    outputDatabase.Set(key, std::to_string(done));
                }
            }

            if (debug) {
                if (returned.function < 0) {
                    std::cout << "{" << returned.sector << ", " << 0 << ", "
                        << temp1 << ", " << temp2 << ", "
                        << temp1_im << ", " << temp2_im << "},\n";
                } else {
                    std::cout << "{" << returned.sector << ", " << returned.function << ", "
                        << temp1 << ", " << temp2 << ", "
                        << temp1_im << ", " << temp2_im << "},\n";
                }
            }
        }  // j
        if (!currentOnlyParse) {
            while ((point != 10) && (i >= points[point])) {
                if (direct) {
                    if (!debug) {
                        std::cout << ".";
                        std::cout.flush();
                    }
                } else {
                    // making special file for Mathematica
                    char prefix_corrected[256];
                    sprintf(prefix_corrected, "%s", currentPrefix.c_str());
                    char* pp = prefix_corrected;
                    while (*pp != '\0') {
                        if (*pp == '/') *pp = '|';
                        pp++;
                    }
                    auto fileName = Format("%s-%s-%s-%s%d", outputDatabaseName.c_str(), currentTask.c_str(), prefix_corrected, (currentOnlyEstimate ? "e" : ""), point);
                    // TODO: replace with std::fstream
                    auto fd = open(fileName.c_str(), O_RDWR | O_CREAT | O_SYNC | O_TRUNC, S_IRUSR | S_IWUSR);
                    if (fd == -1) {
                        throw FiestaException(Format("Failed to open file %s", fileName.c_str()));
                    }
                    if (write(fd, ".", strlen(".")) <= 0) {
                        close(fd);
                        throw FiestaException("File write error");
                    }
                    fsync(fd);
                    close(fd);
                }
                point++;
            }
        }
    }

    auto time_stopA = std::chrono::steady_clock::now();

    // final result for prefix
    std::string result;
    if (complexMode) {
        result = Format("{%.15f,%.15f,%.15f,%.15f}\n", res1, sqrt(res2), res1_im, sqrt(res2_im));
    } else {
        result = Format("{%.15f,%.15f,0,0}\n", res1, sqrt(res2));
    }

    // we will be combining and summing results here

    if (!currentOnlyParse) {
        if (direct) {  // MPI version should be only direct
            std::string key = currentTask + "-" + currentPrefix + "-R";
            if (!debug) {
                std::cout << std::chrono::duration<double>(time_stopA - time_startA).count() << " seconds";
            }
            if (resultsMode != SaveMode::noDatabase && !currentOnlyEstimate) {
                if (onlyPrepare) {
                    outputDatabase.Add(key, "{Print[\"WARNING: database has no result for " + currentPrefix + "\"];0.,0,0,0}\n");
                } else {
                    outputDatabase.Set(key, result);
                }
                outputDatabase.Set(currentTask + "-" + currentPrefix + "-E", "False");
            }

            if (!debug) {
                if (PoolCommon::printStatistics && PoolCommon::termsCount) {
                std::cout << std::endl << "Average number of sampling points (all/MPFR): " << (PoolCommon::pointsCount / PoolCommon::termsCount)
                    << "/" << (PoolCommon::pointsMPFRCount / PoolCommon::termsCount);
            }
                std::cout << std::endl << "Result: " << res1 << " +- " << sqrt(res2);
            } else {
                std::cout << "{0," << res1 << "," << sqrt(res2);
            }
            if (!currentOnlyEstimate) {
                results.emplace(
                    std::pair{currentOrder, currentPrefixSmallVariable},
                    std::pair{res1, sqrt(res2)});
            }

            if (complexMode && (res1_im != 0)) {
                if (!debug) {
                    std::cout << " + ( " << res1_im << " +- " << sqrt(res2_im) << " ) * I";
                } else {
                    std::cout << "," << res1_im << "," << sqrt(res2_im) << "}";
                }
                if (!currentOnlyEstimate) {
                    resultsImaginary.emplace(std::pair{currentOrder, currentPrefixSmallVariable}, std::pair{res1_im, sqrt(res2_im)});
                }
            }

            if (debug) {
                std::cout << "}\"";
            }
            std::cout << std::endl;
            std::cout.flush();

            if (resultsMode == SaveMode::current && shouldReceive && !currentOnlyEstimate) {
                for (int i = firstSector; i <= lastSector; i++) {
                    if (separateTerms) {
                        key = currentTask + "-" + currentPrefix + "-" + std::to_string(i);
                        auto value = inputDatabase.Get(key);
                        int sectorFunctions = std::stoi(value);
                        for (int j = 1; j <= sectorFunctions; j++) {
                            outputDatabase.Remove(currentTask + "-" + currentPrefix + "-" + std::to_string(i) + "-" + std::to_string(j));
                        }
                    } else {
                        outputDatabase.Remove(currentTask + "-" + currentPrefix + "-" + std::to_string(i) + "-" + "0");
                    }
                }
            }

            if (inputPrefix == "" && !currentOnlyEstimate) {
                // need to combine results from different prefixes
                for (auto itr = prefixesSmallVariable.begin(); itr != prefixesSmallVariable.end(); ++itr) {
                    if (prefixesSmallVariable.size() != 1) {
                        char temp[256];
                        strcpy(temp, itr->c_str());
                        char* temp1 = temp;
                        temp1++;
                        char* temp2 = temp1;
                        while (*temp2 != ',') temp2++;
                        *temp2 = '\0';
                        temp2++;
                        while (*temp2 == ' ') temp2++;
                        char* temp3 = temp2;
                        while (*temp3 != '}') temp3++;
                        *temp3 = '\0';
                        std::cout << expandVar << "^(" << temp1 << ") * Log[" << expandVar << "]^(" << temp2 << ") * (";
                    }
                    int minDeg = std::stoi(prefixes.front());
                    auto value = inputDatabase.Get(currentTask + "-" + "SHIFT");
                    int SHIFT = std::stoi(value);
                    for (int j = minDeg - SHIFT; j <= currentOrder - SHIFT; j++) {
                        double result3 = 0;
                        double result4 = 0;
                        double result3_im = 0;
                        double result4_im = 0;
                        for (int i = minDeg; i <= j + SHIFT; i++) {
                            key = currentTask + "-EXTERNAL-" + std::to_string(j - i);
                            inputDatabase.Get(key, &value);
                            if (resultsMode != SaveMode::noDatabase) {
                                outputDatabase.Set(key, value);
                            }
                            auto pos_exp = value.find("*^");
                            if (pos_exp != std::string::npos) {
                                value = value.replace(pos_exp, 2, "E");
                            }
                            double dext = std::stod(value);
                            auto fitr = results.find(std::pair{i, *itr});
                            if (fitr != results.end()) {
                                result3 += ((fitr->second.first) * (dext));
                                result4 += (((fitr->second.second) * (fitr->second.second)) * (dext) * (dext));
                            }
                            if (complexMode) {
                                auto fitr_im = resultsImaginary.find(std::pair{i, *itr});
                                if (fitr_im != resultsImaginary.end()) {
                                    result3_im += ((fitr_im->second.first) * (dext));
                                    result4_im += (((fitr_im->second.second) * (fitr_im->second.second)) * (dext) * (dext));
                                }
                            }
                        }
                        if (result3 != 0) {
                            result4 = sqrt(result4);
                            std::cout << "(" << result3;
                            if (result4 != 0) std::cout << " + pm[" << j << "]*" << result4;
                            std::cout << ") * (ep)^" << j;

                            if ((j != currentOrder - SHIFT) || (complexMode && (result3_im != 0))) {
                                std::cout << " + ";
                            }
                        }
                        if (complexMode) {
                            if (result3_im != 0) {
                                result4_im = sqrt(result4_im);
                                std::cout << "(" << result3_im;
                                if (result4_im != 0) std::cout << " + pm[" << j << "]*" << result4_im;
                                std::cout << ") * I (ep)^" << j;
                                if (j != currentOrder - SHIFT) {
                                    std::cout << " + ";
                                }
                            }
                        }
                    }
                    if (prefixesSmallVariable.size() != 1) {
                        std::cout << " ) ";
                    }
                    std::cout << std::endl;
                }
            }
        } else {
            char prefix_corrected[512];
            sprintf(prefix_corrected, "%s", currentPrefix.c_str());
            char* pp = prefix_corrected;
            while (*pp != '\0') {
                if (*pp == '/') *pp = '|';
                pp++;
            }

            auto fileName = Format("%s-%s-%s-%sR", outputDatabaseName.c_str(), currentTask.c_str(), prefix_corrected, (currentOnlyEstimate ? "e" : ""));

            // TODO: change to std::fstream
            auto fd = open(fileName.c_str(), O_RDWR | O_CREAT | O_SYNC | O_TRUNC, S_IRUSR | S_IWUSR);
            if (fd == -1) {
                throw FiestaException(Format("Failed to open file %s", fileName.c_str()));
            }
            if (write(fd, result.c_str(), result.size()) <= 0) {
                close(fd);
                throw FiestaException("File write error");
            }
            fsync(fd);
            close(fd);
        }
    }
}

void PoolCommon::PreparePrefixIntegration() {
    PoolCommon::pointsCount = 0;
    PoolCommon::pointsMPFRCount = 0;
    PoolCommon::termsCount = 0;

    if (direct && (!currentOnlyParse) && (!debug) && (currentOnlyEstimate || !balanceSamplingPoints)) {
        std::cout << "Terms of order " << currentPrefix << ": ";
        std::string key = currentTask + "-" + currentPrefix + "-T" + (separateTerms ? "" : "0");
        auto value = inputDatabase.Get(key);
        std::cout << value;

        if ((resultsMode != SaveMode::none) && (resultsMode != SaveMode::noDatabase)) {
            key = currentTask + "-" + currentPrefix + "-" + "done";
            if (outputDatabase.TryGet(key, &value)) {
                std::cout << ", done: " << value;
            }
        }

        std::cout << ", max vars: ";
        key = currentTask + "-" + currentPrefix + "-" + "N";
        inputDatabase.Get(key, &value);
        std::cout << value << std::endl;
    }

    std::string key = currentTask + "-" + currentPrefix + "-" + "N";
    inputDatabase.Get(key, &currentNumberOfVariables);

    submitterThread = std::thread(Submitter);
    receiverThread = std::thread(Receiver);
}

void PoolCommon::FinishPrefixIntegration() {
    submitterThread.join();
    receiverThread.join();

    if (direct && !currentOnlyParse && (resultsMode != SaveMode::noDatabase)) {
        size_t pos = currentPrefix.find("-", 1);
        outputDatabase.Set(currentTask + "-" + "MaxEvaluatedOrder", currentPrefix.substr(0, pos));
    }
}

pid_t PoolCommon::StartGuard(char* name) {
    int fdin[2];
    pid_t childpid;
    if (pipe(fdin) < 0) {
        perror("pipe");
        return 0;
    }
    if ((childpid = fork()) == -1) {
        perror("fork");
        return 0;
    }

    if (childpid == 0) {
        /* Child process closes up input side of pipe */
        close(fdin[1]);
        /*Now we may read from fdin[0]*/
        signal(SIGPIPE, SIG_IGN); /*Survive on the pipe closing*/
        /*Use childpid as a buffer -- we need not it anymore:*/

        strcpy(name, "Guard");

        if (read(fdin[0], &childpid, sizeof(pid_t)) < 0) {
            perror("read");
            return false;
        }
        /*The father newer writes to the fdin[1] so if we here
          then the father is dead.*/
        for (int threadIndex = 0; threadIndex != threadsNumber; ++threadIndex) {
            kill(pidCIntegrate[threadIndex], SIGKILL);
        }

        exit(0);
    }

    /* Parent process closes up output side of pipe */
    close(fdin[0]);
    return childpid;
}

void PoolCommon::StartCIntegrate(int count) {
    pipeToCIntegrate.resize(count);
    pipeFromCIntegrate.resize(count);
    pidCIntegrate.resize(count);
    for (int threadIndex = 0; threadIndex != count; threadIndex++) {
        pipeToCIntegrate[threadIndex] = nullptr;
        pipeFromCIntegrate[threadIndex] = nullptr;
        pidCIntegrate[threadIndex] = OpenProgram(&pipeToCIntegrate[threadIndex], &pipeFromCIntegrate[threadIndex], binaryPath);
#ifdef THREADSMODE
        // in MPI mode GPUCore and GPUMemoryPart are set in another place
        if (gpuMode) {
            int realGPUThreadsPerNode = GPUThreadsPerNode;
            if (realGPUThreadsPerNode > count) {
                realGPUThreadsPerNode = count;
                // cannot have more GPU threads than threads;
            }
            if (threadIndex < realGPUThreadsPerNode) {
                GPUCore = threadIndex % GPUPerNode;
                GPUMemoryPart = realGPUThreadsPerNode / GPUPerNode;
                if ((threadIndex % GPUPerNode) < (realGPUThreadsPerNode % GPUPerNode)) {
                    GPUMemoryPart++;
                    // for example, we have GPUPerNode = 3 and realGPUThreadsPerNode = 5. Then GPUMemoryPart will be 2 2 1 2 2 (depending on threadIndex)
                }
            } else {
                // switching off GPU
                GPUCore = -1;
                GPUMemoryPart = 1;
            }
        }
#endif
        try {
            InitCIntegrateParameters(pipeToCIntegrate[threadIndex], pipeFromCIntegrate[threadIndex]);
        } catch (const std::exception& exception) {
            std::cout << "Failed to init integrator no. " << threadIndex << ", error: " << exception.what();
            kill(pidCIntegrate[threadIndex], SIGKILL);
            waitpid(pidCIntegrate[threadIndex], nullptr, 0);
        }
    }
}

void PoolCommon::StopCIntegrate(int count) {
    if (pipeToCIntegrate.empty()) {
        return;
    }
    for (int threadIndex = 0; threadIndex != count; threadIndex++) {
        fputs("Exit\n", pipeToCIntegrate[threadIndex]);
        fflush(pipeToCIntegrate[threadIndex]);
        waitpid(pidCIntegrate[threadIndex], nullptr, 0);
        fclose(pipeToCIntegrate[threadIndex]);
        fclose(pipeFromCIntegrate[threadIndex]);
    }
}

long long PoolCommon::balancedMaxeval(int sector, int function) {
    long long maxeval = stoll(intpar["maxeval"]);
    long long currentMaxeval;
    if (!currentOnlyEstimate && balanceSamplingPoints) {
        double currentPart = estimatedIntegrals[std::pair{sector, function}];
        currentMaxeval = maxeval * currentPart;
        if (currentMaxeval < maxeval / minimalSamplingPart) {
            currentMaxeval = maxeval / minimalSamplingPart;
        }
    } else if (currentOnlyEstimate) {
        currentMaxeval = maxeval / estimationPart;
    } else {
        currentMaxeval = maxeval;
    }
    return currentMaxeval;
}

long long PoolCommon::RunIntegration(int threadIndex, const char* integrand, long long currentMaxeval,  char* result) {
    // we do not need to crash here, if the pipe is broken we will receive SIGPIPE anyway
    char* answerFromPipe;

    if (currentMaxeval) {
        Execute(pipeToCIntegrate[threadIndex], pipeFromCIntegrate[threadIndex], "SetCurrentIntegratorParameter", std::string("maxeval"), currentMaxeval);
    }
    char buf[256];
    fputs("ClearStatistics\n", pipeToCIntegrate[threadIndex]);
    fflush(pipeToCIntegrate[threadIndex]);
    answerFromPipe = fgets(buf, 256, pipeFromCIntegrate[threadIndex]);
    if (answerFromPipe == nullptr) {
        throw FiestaException("No clear statistics answer, pipe is broken");
    }
    if (currentOnlyParse) {
        fputs("Parse\n", pipeToCIntegrate[threadIndex]);
    } else {
        fputs("Integrate\n", pipeToCIntegrate[threadIndex]);
    }
    fputs(integrand, pipeToCIntegrate[threadIndex]);
    fflush(pipeToCIntegrate[threadIndex]);
    answerFromPipe = fgets(result, 1023, pipeFromCIntegrate[threadIndex]);
    if (answerFromPipe == nullptr) {
        throw FiestaException("No answer, pipe is broken");
    }
    fputs("Statistics\n", pipeToCIntegrate[threadIndex]);
    fflush(pipeToCIntegrate[threadIndex]);
    answerFromPipe = fgets(buf, 256, pipeFromCIntegrate[threadIndex]);
    if (answerFromPipe == nullptr) {
        throw FiestaException("No statistics answer, pipe is broken");
    }
    unsigned long long points1, points2;
    if (sscanf(buf, "Native points: %llu, MPFR points: %llu", &points1, &points2) != 2) {
        throw FiestaException("Incorrect statistics returned");
    }
    termsCount++;
    pointsCount += (points1 + points2);
    pointsMPFRCount += (points2);
    return (points1 + points2);
}

void PoolCommon::RunIntegrationTest() {
    char res[512];
    PoolCommon::StartCIntegrate(1);
    std::cout << "Ok" << std::endl;
    fputs("GetCurrentIntegratorParameters\n", PoolCommon::pipeToCIntegrate[0]);
    fflush(PoolCommon::pipeToCIntegrate[0]);
    if (fgets(res, 512, PoolCommon::pipeFromCIntegrate[0]) == nullptr) {
        throw FiestaException("GetCurrentIntegratorParameters() failed");
    }
    std::cout << res << std::endl;
    fputs("Integrate\n1;\n0;\nx[1]+1;\n|\n", PoolCommon::pipeToCIntegrate[0]);
    fflush(PoolCommon::pipeToCIntegrate[0]);
    auto answerFromPipe = fgets(res, 200, PoolCommon::pipeFromCIntegrate[0]);
    if (answerFromPipe == nullptr) {
        throw FiestaException("Failed to get response from Integrate");
    }
    if ((std::string(res, 4) != "{1.5") && (std::string(res, 4) != "{1.4")) {
        throw FiestaException("Integration failed");
    }
    PoolCommon::StopCIntegrate(1);
}
