;
vars = Variables[result];
vars = Select[vars,
   Or[StringCases[ToString[##], "PM"] =!= {},
     StringCases[ToString[##], "pm"] =!= {}] &];
result = result /.
   Rule @@@ Transpose[{vars, Table[0, {Length[vars]}]}];
result = result /. {Epsilon -> ep, Ep -> ep};
result = Rationalize[Chop[result, 0.00001],0.00001];
Print[InputForm[result]];
Print[InputForm[N[result]]];
Print[InputForm[etalon]];
Print[InputForm[N[etalon]]];
diff = etalon - result;
If[Head[diff] =!= List, diff = {diff}];
diff = Expand[Union[diff]];
Print[InputForm[diff]];
If[diff =!= {0},
    WriteString[Streams["stderr"], "Test failed!\n"];
,
    WriteString[Streams["stderr"], "Ok\n"];
];
