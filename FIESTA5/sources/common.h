/** @file Common.h
 *
 *  This file is a part of the FIESTA package
 *
 *  This file contains global definitions and variables.
 */

#pragma once

#include <stdio.h>

#include <chrono>
#include <string>
#include <unordered_map>
#include <vector>

#ifdef GPU
#include <cuda_runtime.h>
#endif

/**
 * Type of arithmetics used. Mixed is auto-detection.
 */
enum class ArithmeticsMode {mixed, native, MP};

/**
 * All possible mathematical Operations supported by FIESTA
 */
enum class Operation : char {
    PLUS,
    MUL,
    MINUS,
    DIV,
    NEG,
    INV,
    LOG,
    CPY,
    IPOW2,
    IPOW3,
    IPOW4,
    IPOW5,
    IPOW6,
    IPOW,
    POW
};

/**
 * Class with static global mebmers Common for all code
 */
#ifndef NVCC
class Common {
    public:
        inline static std::string mathBinary = {}; /**<If set, path to the math binary which can be called to evaluate some functions*/
        inline static size_t allTriads = 0; /**<Counter of all triads encountered in the integrand*/
        inline static size_t usedTriads = 0; /**<Counter of really used triads in the integrand with optimization*/
        inline static size_t memoryTetrads = 0; /**<Counter of memory used Runtime tetrads after reusage*/
        inline static bool withOptimization = true; /**<Indication whether to perform Triad optimization, finding equivalents*/
        inline static int statistics[2] = {}; /**<Statistics of the number of sampling points used*/
        inline static long long timeStatistics[6] = {}; /**<Time usage statistics*/
        inline static bool testF = false; /**<If true, a special mode is used not integrating vbut checking the sign of F (from alpha-rep). Answer is meaningless, the point is not to crash*/
        inline static bool debug = false; /**<Turns on debug mode printing values of integrand in all sampling points*/
        inline static ArithmeticsMode arithmetics = ArithmeticsMode::mixed; /**<How to evaluate, native MP or mixed*/
        inline static int CPUBatch = 128; /**<Number of sampling point in a batch for a working CPU core*/
        inline static int GPUBatch = 0; /**<Number of sampling point in a batch for a working GPU core*/
#ifdef GPU
        inline static int CPUCores = 0; /**<Number of working CPU cores, now 0 or 1 in GPU mode, 1 in CPU mode*/
        inline static int GPUCores = 1; /**<Number of working GPU cores, now 0 or 1 in GPU mode, 0 in CPU mode*/
#else
        inline static int CPUCores = 1; /**<Number of working CPU cores, now 0 or 1 in GPU mode, 1 in CPU mode*/
        inline static int GPUCores = 0; /**<Number of working GPU cores, now 0 or 1 in GPU mode, 0 in CPU mode*/
#endif

        inline static double MPsmallX = 0.001; /**<X value where to calculate the worst product to detect its size and MP mode precision*/
        inline static double MPthreshold = 1E-9; /**<The value of the worst monom, so that if it is bigger, we can work with nativw arithmetics*/
        inline static int MPprecisionShift = 38; /**<Additional bits to compared to what we need for the worst monom to define MP precision, has a default value*/
        inline static double userSetMPMin = -1.; /**<If set, used to define a point where precision should be even more, than the one chosen for worst monom*/
#ifdef GPU
        inline static int cudaSM = 0; /**<Number of CUDA streaming multiprocessors in the current GPU, used to determine GPU batch*/
        inline static long long int cudaMemoryLimit = 0; /**<Free memory in the current GPU, used to restrict the size of GPU batch if not enough*/
#endif
        inline static int GPUMemoryPart = 1;  /**<The part of GPU memory that this process is allowed to use (interpreted as 1/GPUMemoryPart); 1 (all memory) by default*/
        inline static bool avx_enabled = true; /**<True by default and False if command NoAVX is there*/
        inline static int batchSize; /**<The number of sampling points evaluated a time*/
};
#endif

constexpr size_t maxVariables = 32; /**< We cannot imagine more than a 32-dimensional integral, but it can be safely increased if needed*/
constexpr double absMonomMin = 1.0E-308l; /**<Cannot handle smaller values, should not be normally changed*/

#ifdef COMPLEX
constexpr int complexMultiplier = 2; /**<Multiplyer for batch sized depending on whether we use complex numbers or not*/
#else
constexpr int complexMultiplier = 1; /**<Multiplyer for batch sized depending on whether we use complex numbers or not*/
#endif
