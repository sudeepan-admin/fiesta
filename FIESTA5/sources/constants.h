/** @file constants.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIESTA package
 *  It contains the definitions for predefined constants such ad Pi with high precision.
 *  (the values of constants can be found in constants.cpp)
 *
 */

#pragma once

#include <string>
#include <unordered_map>

/**
 * @brief This map stores correspondence of human-understandable names such as "Pi" to values with a 4096 digit precision.
 */
static std::unordered_map<std::string, std::string> predefinedConstants;

/**
 * Finds the value of a predefined constant, aborts is not found
 * @param key Constant name such as "Pi"
 * @return the 4096 digit precision value
 */
std::string GetPredefinedConstant(std::string key);

/**
 * Sets values for predefined constants, should be called once on program initialization.
 */
void InitializePredefinedConstants();
