/** @file Scanner.h
 *
 *  This file is a part of the FIESTA package
 *
 *  The main function is parseIntegrand which takes a string and parses the expression,
 *  starting from the number of variables, the number of auxiliary functions, those functions
 *  and the final expression.
 *
 *  As a result we are ready to evaluate the expression in a direct cycle
 *  looking at current operation, having addresses of operands and the address where to put the result.
 */

#pragma once

#include <functional>
#include <memory>
#include <cstdlib>
#include <deque>
#include <stack>
#include <vector>
#include <unordered_map>
#include <string>
#include <sstream>
#include <mpfr.h>
#include <stdio.h>

#include "common.h"

#ifdef ENABLE_ICC
#include <aligned_new>
#endif

/**
 * Class for storing numbers in multiprecision mode
 */
class MPfloat {
    public:
        mpfr_t re; /**<The real part*/
#ifdef COMPLEX
        mpfr_t im; /**<The imaginary part in complex mode*/
#endif
};

/**
 * Types of operands, which can be x (variable), f (auxiliary function), constant (fixed number read with the parser), Triad (calculated expression) and integer (for integer power)
 */
enum class OperandType : uint8_t {
    x,
    f,
    constant,
    Triad,
    integer,
};

/**
 * Operands obtained during parsing
 */
class Operand {
    public:
        OperandType operandType; /**<One of the Operand types*/
        unsigned int operandNumber; /**<The number of the Operand among the chosen type. x and f are numbered from 1, constants and triads from 0, with integer type it is just the parsed number*/
        
        
        Operand() = default;
        
        /**
         * Basic constructor defining Operand type and number
         * @param operandType the type
         * @param operandNumber the number
         */
        Operand(const OperandType &operandType, const unsigned int operandNumber) : operandType(operandType), operandNumber(operandNumber) {};
        
        friend bool operator==(const Operand& op1, const Operand& op2);
        friend bool operator!=(const Operand& op1, const Operand& op2);
        friend bool operator<(const Operand& op1, const Operand& op2);
        friend bool operator>(const Operand& op1, const Operand& op2);
};

/**< Type used partially in operation numbering (but in other ints too). Should be signed do to the way operands are numbered.*/



/**
 * @brief Class defining a compile-time Triad.
 *
 * The class has an operation and two operands.
 */
class Triad {
    public:
        Operation operation; /**< operation such as MUL and other*/
        Operand firstOperand; /**< first Operand*/
        Operand secondOperand; /**< second Operand for non-unary Operations*/
   
        friend bool operator==(const Triad& c1, const Triad& c2);
        friend bool operator!=(const Triad& c1, const Triad& c2);
};

namespace std {
    /**
     * Defined hash function for the Triad class in the std namespace to use them in unordered maps
     */
    template<>
    struct hash<Triad> {
        /**
         * The hash function for a Triad itself
         * @param c Triad needing a hash
         * @return the hash value
         */
        inline size_t operator()(const Triad& c) const {
            return (static_cast<size_t>(c.firstOperand.operandNumber)<<32) +
                (static_cast<size_t>(c.secondOperand.operandNumber)<<8) +
                static_cast<uint8_t>(c.operation);
        }
    };
}

/**
 * Special struct for passing info about a term in algebraic sum counting different parameters.
 * Used for sorting in parseAlgebraicSum
 */
class TermInfo {
    public:
        Operand addr; /**< The address of the expression*/

        size_t nTriads; /**< Number of triads used in the product*/
        size_t nMul; /**< Number of multiplications used in the product*/
        size_t nDiv; /**< Number of divisions used in the product*/
        size_t nPow; /**< Number of powers used in the product*/
        size_t nLog; /**< Number of logarithms used in the product*/
};

/**
 * @brief One of the main classes, performs parsing of the integrand string.
 */
class Scanner {
    private:

        std::string integrand; /**<String with the integrand expression that is being parsed*/
        const char *theChar = {}; /**<Pointer to the buffer of integrand*/

        std::vector<Triad> compileTimeTriads; /**<Vector of triads - ready Operations for evaluation after translation to Runtime*/
        std::unordered_map<Triad, Operand> triadHashTable; /**<Search table for triads not to dublicate them. Not used if optimization is off*/

        unsigned int nx = {}; /**<number of coordinates + 1*/
        unsigned int nf = {}; /**<number of intermediate functions + 1 (!!!!!)*/

        std::vector<MPfloat> MPx; /**<vector of MP coordinates*/

        static constexpr std::align_val_t AVXAlign { 256 }; /**<Alignment for proper AVX operations*/
        /**
         * This function delete memory allocated with aligned new
         * @param arr The memory to be deallocated
         * @return void
         */
        static void alignedDeleter (double* arr) {
            operator delete[] (arr, AVXAlign);
        };
        std::unique_ptr<double[], decltype(&alignedDeleter)> nativeX = {nullptr, &alignedDeleter}; /**<vector of a batch of native coordinates*/

        /**
         * This container stores all addresses of MPFR variables appearing after parsing.
         * They contain links to, constants and Runtime triads (f included)
         * It is used it runline to change MPFR precision everywhere
         */
        std::deque<MPfloat*> allMPAddresses;
        /**
         * The Triad addresses of intermediate functions.
         */
        std::vector<Operand> f;
        /**
         * maxX[i] in a maximal negative power of a bare x[i]
         * Used to determine whether to use MP arithmetics
         */
        std::vector<unsigned int> maxX;
        /**
         * Constants read out from the integrand as they are, a source for nativeConstants and MPConstants
         * An empty string means a constant inverse to the previous one
         * Only 0 and I are excluded.
         */
        std::deque<std::string> stringConstants;
        /**
         * Addresses of native constants read out from the integrand
         */
        std::deque<std::unique_ptr<double[], decltype(&alignedDeleter)>> nativeConstants;
        /**
         * Addresses of MP constants read out from the integrand
         */
        std::vector<MPfloat> MPConstants;
        /**
         * After scanning this vector is filled with addresses of native triads used during calculation (not x or f or constants)
         */
        std::vector<std::unique_ptr<double[], decltype(&alignedDeleter)>> nativeTriadAddresses;
        /**
         * Inverse to stringConstants, finds a constant number by its string
         */
        std::unordered_map<std::string, Operand> constantOperands;

        /**
         * Stack of operands during parsing
         * >=0 -- triads
         *-nx..-1 -- x
         * -nf-nx ... -nx -- f
         * < -nf-nx -- float constants read from the integrand
         */
        std::stack<Operand> operandStack;

        /**
         * Size of a block of Triad addresses, was initially used for cache optimization, but not sure whether this is important
         */
        static constexpr int triadBlockSize = 16;

        /**
         * The function returns the current character corresponding to *theChar,
         * ignoring white spaces (<=' ' ), and sets the pointer theChar to the next position.
         * @return The first real symbol after spaces and such
         */
        char NextChar();

        /**
         * Parses an integer from input.
         * @param n address where to put the number
         * @param e can be nullptr, if not, address of where to put 10^the number of digits of the number
         * @return next char in the input after the number
         */
        char ParseInt(unsigned int *n, int* e = nullptr);

        /**
         * Parses an integer from input without converting in into a number.
         * Stops when encountering a non-number.
         * @param ss Stream to which the read expression is appended.
         * @return next char in the input after the number
         */
        char StringParseInt(std::stringstream& ss);

        /**
         * Parses exponent in p[a, b];
         * Is called when "," was already scanned.
         * @param xVar either 0 or i in case a = x[i]; In this case this function updates the max negative power of x appearing in the expression.
         * @return next char in the input after scanning the power (after possible spaces we expect ']' there).
         */
        char ParseExponent(unsigned int xVar);

        /**
         * Parses a PolyGamma from input;
         * Is called when "Po" was already scanned.
         * @return next char in the input after the polygamma.
         */
        char ParsePolyGamma();

        /**
         * Parses an algebraic product - a number of terms separated with multiplication and division.
         * This function is called by parseAlgebraicSum
         * This function can call parseAlgebraicSum when encountering power, log or brackets.
         * After encountering power and after the call of parseAlgebraicSum in also calls parseExponent.
         * This function does not put operands to the stack directly, but first collects them all and puts in a special order:
         * first x, then constants, then f, then powers, then logarithms, then expressions from brackets.
         * This function increases Operand stack exactly by one Operand.
         * @param term special struct to return information about the term parsed such as number of powers and such.
         * @param operation Pointer to an operation which is OP_PLUS before call, but can be changed to OP_MINUS in case the expression starts from -, the general sign
         * @return the symbol after the parsing of the algebraic product is over, a closing bracket or  similar
         */
        char ParseAlgebraicProduct(TermInfo& term, Operation *operation);

        /**
         * Parses an algebraic sum - a number of terms separated with addition and substraction.
         * This function is called by parseIntegrand or parseAlgebraicProduct.
         * This function calls parseAlgebraicProduct for all terms between - and +, then pops the result from the Operand stack.
         * This function does not put terms to the stack apon encountering but first collects all then sorts them, and only then adds to the stack.
         * @return the symbol after the parsing of the algebraic sum is over, a closing bracket or similar.
         */
        char ParseAlgebraicSum();

        /**
         * Tests whether the existing tripple forms a new Triad or an existing one.
         * In case it is a new one, it is added to the hash table and to the vector of compiled triads.
         * @param op operation
         * @param operand1 first Operand
         * @param operand2 second Operand for non-unary
         * @return Triad as an Operand, either new one, or the one found.
         */
        Operand TestAddTriad(Operation op, Operand operand1, Operand operand2);

        /**
         * Adds an evaluation operation to the scan result.
         * This function pops 1 or 2 operands from the Operand stack (depending on whether the operation in unary or not).
         * Then it creates a new Triad or finds an existing if this one is a copy and pushes its number to the Operand stack.
         * @param op The operation being added.
         */
        void AddOperation(Operation op);

#if defined(COMPLEX) || defined(DOXYGEN_DOCUMENTATION)
        /**
         * Adds a native number to the list of constants.
         * This function fills an arrar with equal constants for fast evaluation.
         * @param re the real part of the constant
         * @param im the imaginary part of the constant, only in complex mode, normally 0., but we have I
         */
        void AddFloatConstant(double re, double im = 0.);
#else
        void AddFloatConstant(double re);
#endif

        /**
         * Initializes an MPFR variable and adds it to the set of variables, so that it will be easy to reset its precision or clear
         * @param mp the address of the variable being added
         */
        void InitMPVariable(MPfloat *mp);

        /**
         * Adds a new constant to the constants pool.
         * A constant is stored as a string, as an array of equal native floats and as an MP value
         * @param key The key for the constants map not to set equal constants. Almost coinsides with what is read from the integrand. Can have a preceeding ! indicating a request for math.
         * @param val One of the following: a number (valid for a double or a fraction of doubles); I (complex); expression starting with ! to be sent to Mathematica for evaluation
         * @param invAlso If true, stores an inverse of the constant after it, used for replacing division with multiplication.
         * @return the number of the constant starting from 0.
         */
        Operand AddConstant(const std::string& key, const std::string& val, bool invAlso);

        /**
         * Tests whether a constant defined by str is already in the constant pool. If not, calls addConstant.
         * @param key The key for the constants map not to set equal constants. Almost coinsides with what is read from the integrand. Can have a preceeding ! indicating a request for math.
         * @param val Either a value in case a predefined constant is used or an empty string - in this case str is used as information for evaluation.
         * @param invAlso If true, stores an inverse of the constant after it, used for replacing division with multiplication.
         * @return the number of the constant starting from 0.
         */
        Operand TestAddConstant(const std::string& key, const std::string& val, bool invAlso);

    public:
        ~Scanner();

        /** Public method to see integral dimension
         * @return dimension of the integral
         */
        unsigned int GetDimension() {
            return nx - 1;
        }

        /**
         * Parses an integrand expression.
         * First reads the number of x (coordinates).
         * Then reads the number of f (auziliary functions).
         * Then calls parseAlgebraicSum for each of f.
         * Then calls parseAlgebraicSum for the final expression.
         * @param s The string with integrand.
         */
        void ParseIntegrand(const std::string& s);

    friend class Runtime; /**< The runtime class should be able to access members */
};
