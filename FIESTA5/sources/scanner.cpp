/** @file Scanner.cpp
 *
 *  This file is a part of the FIESTA package
 *
 *  The main function is ParseIntegrand which takes a string and parses the expression,
 *  starting from the number of variables, the number of auxiliary functions, those functions
 *  and the final expression.
 *
 *  As a result we are ready to evaluate the expression in a direct cycle
 *  looking at current operation, having addresses of operands and the address where to put the result.
 */

#include "scanner.h"

#include <stack>
#include <algorithm>
#include <list>
#include <map>
#include <vector>
#include <deque>
#include <tuple>
#include <mpfr.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include "common.h"
#include "constants.h"
#include "../sourcesCommon/util.h"

static bool IsOperationBinaryCommutative(Operation operation) {
    switch (operation) {
        case Operation::PLUS:
            return true;
        case Operation::MUL:
            return true;
        case Operation::MINUS:
            return false;
        case Operation::DIV:
            return false;
        case Operation::NEG:
            return false;
        case Operation::INV:
            return false;
        case Operation::LOG:
            return false;
        case Operation::CPY:
            return false;
        case Operation::IPOW2:
            return false;
        case Operation::IPOW3:
            return false;
        case Operation::IPOW4:
            return false;
        case Operation::IPOW5:
            return false;
        case Operation::IPOW6:
            return false;
        case Operation::IPOW:
            return false;
        case Operation::POW:
            return false;
    }
    FIESTA_FAIL("Unreachable");
}

static bool IsOperationUnary(Operation operation) {
    switch (operation) {
        case Operation::PLUS:
            return false;
        case Operation::MUL:
            return false;
        case Operation::MINUS:
            return false;
        case Operation::DIV:
            return false;
        case Operation::NEG:
            return true;
        case Operation::INV:
            return true;
        case Operation::LOG:
            return true;
        case Operation::CPY:
            return true;
        case Operation::IPOW2:
            return true;
        case Operation::IPOW3:
            return true;
        case Operation::IPOW4:
            return true;
        case Operation::IPOW5:
            return true;
        case Operation::IPOW6:
            return true;
        case Operation::IPOW:
            return false;
        case Operation::POW:
            return false;
    }
    FIESTA_FAIL("Unreachable");
}

/**
 * Special equaity comparisson for triads.
 * Takes into account that some Operations need no second Operand and some are commuting
 * @param c1 First Triad
 * @param c2 Second Triad
 * @return whether they are equal
 */
bool operator==(const Triad& c1, const Triad& c2) {
    if (c1.operation != c2.operation) {
        return false;
    }
    return std::tie(c1.firstOperand, c1.secondOperand) == std::tie(c2.firstOperand, c2.secondOperand) ||
        (IsOperationBinaryCommutative(c1.operation) &&
         std::tie(c1.secondOperand, c1.firstOperand) == std::tie(c2.firstOperand, c2.secondOperand));
}

/**
 * Non-equaity comparisson for triads.
 * Takes into account that some Operations need no second Operand and some are commuting
 * @param c1 First Triad
 * @param c2 Second Triad
 * @return whether they are not equal
 */
bool operator!=(const Triad& c1, const Triad& c2) {
    return !(c1 == c2);
}

/**
 * Equaity comparisson for operands
 * @param op1 First Operand
 * @param op2 Second Operand
 * @return whether they are equal
 */
bool operator==(const Operand& op1, const Operand& op2) {
    return std::tie(op1.operandType, op1.operandNumber) == std::tie(op2.operandType, op2.operandNumber);
}

/**
 * Non-equaity comparisson for operands
 * @param op1 First Operand
 * @param op2 Second Operand
 * @return whether they are not equal
 */
bool operator!=(const Operand& op1, const Operand& op2) {
    return !(op1 == op2);
}

/**
 * Smaller comparison for operands
 * @param op1 First Operand
 * @param op2 Second Operand
 * @return whether first is smaller
 */
bool operator<(const Operand& op1, const Operand& op2) {
    return std::tie(op1.operandType, op1.operandNumber) < std::tie(op2.operandType, op2.operandNumber);
}

/**
 * Greater comparison for operands
 * @param op1 First Operand
 * @param op2 Second Operand
 * @return whether first is greater
 */
bool operator>(const Operand& op1, const Operand& op2) {
    return std::tie(op1.operandType, op1.operandNumber) > std::tie(op2.operandType, op2.operandNumber);
}

Scanner::~Scanner() {
    mpfr_free_cache();

    for (auto &term : allMPAddresses) {
        mpfr_clear(term->re);
#ifdef COMPLEX
        mpfr_clear(term->im);
#endif
    }
}

char Scanner::NextChar() {
    // TODO: Comparison with space looks hacky, review.
    while ((*theChar != '\0') && (*theChar <= ' ')) {
        (theChar)++;
    }
    FIESTA_VERIFY(*theChar != '\0', "Unexpected end of expression evaluation");
    return *(theChar)++;
}

char Scanner::ParseInt(unsigned int *n, int *e) {
    if (e != nullptr) {
        *e = 1;
    }
    for (;;) {
        switch (char c = NextChar()) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                if (e != nullptr) {
                    (*e) *= 10;
                }
                *n = *n * 10 + c - '0';
                break;
            default:
                return c;
        }
    }
}

char Scanner::StringParseInt(std::stringstream& ss) {
    while(true) {
        auto c = NextChar();
        switch (c) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                ss << c;
                break;
            default:
                return c;
        }
    }
}

Operand Scanner::TestAddTriad(Operation op, Operand operand1, Operand operand2) {
    Common::allTriads++;

    Triad t;
    t.operation = op;
    t.firstOperand = operand1;
    t.secondOperand = operand2;

    Operand result(OperandType::Triad, compileTimeTriads.size());

    if (Common::withOptimization) {
        auto itr = triadHashTable.find(t);
        if (itr == triadHashTable.end()) {
            triadHashTable.emplace(t, result);
        } else {
            result = itr->second;
        }
    }

    if (result.operandNumber == compileTimeTriads.size()) { /*A new Triad was added*/
        compileTimeTriads.push_back(t);
        Common::usedTriads++;
    }
    return result;
}

void Scanner::AddOperation(Operation op) {
    Operand op1, op2, Triad;
    FIESTA_VERIFY(!operandStack.empty(), "Stack underflow");
    if (IsOperationUnary(op)) {
        op1 = op2 = operandStack.top();
        operandStack.pop();
    } else {
        op2 = operandStack.top();
        operandStack.pop();
        FIESTA_VERIFY(!operandStack.empty(), "Stack underflow");
        op1 = operandStack.top();
        operandStack.pop();
    }
    Triad = TestAddTriad(op, op1, op2);
    operandStack.push(Triad);
}

void Scanner::InitMPVariable(MPfloat *mp) {
    mpfr_init(mp->re);
#ifdef COMPLEX
    mpfr_init(mp->im);
#endif
    allMPAddresses.push_back(mp);
}

#if defined(COMPLEX) || defined(DOXYGEN_DOCUMENTATION)
void Scanner::AddFloatConstant(double re, double im)
#else
void Scanner::AddFloatConstant(double re)
#endif
{
    auto element = std::unique_ptr<double[], decltype(&Scanner::alignedDeleter)>(
            new(Scanner::AVXAlign) double[complexMultiplier * Common::batchSize],
            &Scanner::alignedDeleter
        );
    for (int j = 0; j != Common::batchSize; ++j) {
        element[j] = re;
    }
#ifdef COMPLEX
    for (int j = Common::batchSize; j != 2*Common::batchSize; ++j) {
        element[j] = im;
    }
#endif
    nativeConstants.push_back(std::move(element));
}

Operand Scanner::AddConstant(const std::string& key, const std::string& val, bool invAlso) {
#ifdef COMPLEX
    if (val == "I") {
        AddFloatConstant(0.,1.);
    } else {
#endif
        if (val[0] == '!') {
            FIESTA_VERIFY(Common::mathBinary != "", "Mathematica binary required but not set");
            int fdin[2], fdout[2];
            FIESTA_VERIFY(pipe(fdin) >= 0, "Cannot start pipe");
            FIESTA_VERIFY(pipe(fdout) >= 0, "Cannot start pipe");
            pid_t childpid = fork();
            FIESTA_VERIFY(childpid != -1, "Error on fork");

            if (childpid == 0) {
                /* Child process closes up input side of pipe */
                close(fdin[1]);
                close(fdout[0]);
                /*reopen stdin:*/
                close(0);
                FIESTA_VERIFY(dup(fdin[0]) >=0, "Error on dup");
                /*reopen stdout:*/
                close(1);
                FIESTA_VERIFY(dup(fdout[1]) >=0, "Error on dup");
                execlp(Common::mathBinary.c_str(), Common::mathBinary.c_str(), "-noprompt", nullptr);
                fprintf(stderr, "%d\n", errno);
                FIESTA_FAIL("Could not launch mathematica");
                exit(0);
            } else { /* Parent process closes up output side of pipe */
                FILE* inputPipe, *outputPipe;
                close(fdin[0]);
                close(fdout[1]);
                FIESTA_VERIFY((inputPipe = fdopen(fdin[1], "w")) != nullptr, "Cannot fdopen");
                FIESTA_VERIFY((outputPipe = fdopen(fdout[0], "r")) != nullptr, "Cannot fdopen");
                char buf[256];
                snprintf(buf, sizeof(buf), "N[%s,4096]\nExit\n", val.c_str() + 1);
                fputs(buf, inputPipe);
                fflush(inputPipe);
                char mathematicaAnswer[8192];
                FIESTA_VERIFY(fgets(mathematicaAnswer, sizeof(mathematicaAnswer), outputPipe) != buf, "Cannot read Mathematica result from pipe");
                for (char* pos = mathematicaAnswer; *pos!='\0'; ++pos) {
                    if (*pos == '`') *pos = '\0';
                }
                FIESTA_VERIFY(
                    mathematicaAnswer[0] != '\0' && mathematicaAnswer[0] != '!',
                    Format("incorrect response from Mathematica for command %s", buf));
                // recursion, but maximally once
                return AddConstant(key, mathematicaAnswer, invAlso);
            }
        } else {
            char *p;
            double r = strtod(val.c_str(), &p);
            FIESTA_VERIFY(
                p != nullptr, 
                Format("could not add a new constant\n, key is (%s)\n val is (%s)", key.c_str(), val.c_str()));
            if (*p == '/') {
                ++p;
                auto d = strtod(p, &p);
                FIESTA_VERIFY(
                    (p != nullptr) && ((*p == '\0') || (*p == '\n')),
                    Format("could not add a new constant\n, key is (%s)\n val is (%s)", key.c_str(), val.c_str()));
                r /= d;
            } else {
                FIESTA_VERIFY(
                    (*p == '\0') || (*p == '\n'),
                    Format("could not add a new constant\n, key is (%s)\n val is (%s)", key.c_str(), val.c_str()));
            }
            AddFloatConstant(r);
            if (invAlso) {
                AddFloatConstant(1.0l / r);
            }
        }
#ifdef COMPLEX
    }
#endif
    size_t cpos = stringConstants.size();
    stringConstants.emplace_back(val);
    if (invAlso) {
        stringConstants.emplace_back();
    }
    Operand res(OperandType::constant, cpos);
    constantOperands.emplace(key, res);
    return res;
}

Operand Scanner::TestAddConstant(const std::string& key, const std::string& val, bool invAlso) {
    auto itr = constantOperands.find(key);
    if (itr != constantOperands.end()) {
        return itr->second;
    }
    return AddConstant(key, val.empty() ? key : val, invAlso);
}

char Scanner::ParsePolyGamma() {
    char c;
    const char *id = "PolyGamma";
    char polyGamma[128];
    unsigned int i, l = strlen(id);
    Operand cpos;

    /*If ID_CONST_POLYGAMMA == "PolyGamma" then "Pol" is scanned, now scan "yGamma":*/
    for (i = 3; i < l; i++) {
        c = NextChar();
        FIESTA_VERIFY(c == id[i], Format("Parse error: %c expected instead of %c\n", id[i], c));
    }
    i = l = 0;
    FIESTA_VERIFY(NextChar() == '[', "'[' expected");
    FIESTA_VERIFY(ParseInt(&i) == ',', "',' expected");
    c = ParseInt(&l);
    if (c == '/') {
        unsigned int l2 = 0;
        FIESTA_VERIFY(ParseInt(&l2) == ']', "']' expected\n");
        sprintf(polyGamma, "!PolyGamma[%u,%u/%u]", i, l, l2);
        cpos = TestAddConstant(polyGamma, "", true);
    } else {
        FIESTA_VERIFY(c == ']', "']' expected\n");
        /*now in i and l we have a weight*/

        if (((i == 1) && (l == 1)) ||
            ((i == 2) && (l == 1)) ||
            ((i == 2) && (l == 2)) ||
            ((i == 2) && (l == 3)) ||
            ((i == 2) && (l == 4)) ||
            ((i == 3) && (l == 1)) ||
            ((i == 3) && (l == 2)))
        {
            sprintf(polyGamma, "PolyGamma[%u,%u]", i, l);
            cpos = TestAddConstant(polyGamma, GetPredefinedConstant(polyGamma), true);
        } else {
            sprintf(polyGamma, "!PolyGamma[%u,%u]", i, l);
            cpos = TestAddConstant(polyGamma, "", true);
        }
    }
    operandStack.push(cpos);
    return NextChar();
}

char Scanner::ParseAlgebraicProduct(TermInfo &term, Operation *operation) {
    enum class Priority {
        Unknown,
        X,
        C,
        F,
        P,
        L,
        Zero,
    };

    // priority, Operand, operation (order is important for comparison).
    std::vector<std::tuple<Priority, Operand, Operation>> diads;
    // operation + Operand
    int dCounter = 0;
    unsigned int n;
    Operation nextOperation = Operation::MUL;
    auto nextPriority = Priority::Unknown;
    bool firstDiad = true;
    for (;;) {
        char c = NextChar();

        /*The Operand:*/
        switch (c) {
            case '-':
                *operation = Operation::MINUS;
                [[fallthrough]];
            case '+': /*ignore leading '+':*/
                continue;
            case '(':
                c = ParseAlgebraicSum();
                FIESTA_VERIFY(c == ')', "')' expected");
                nextPriority = Priority::Zero;
                c = NextChar();
                break;
            case 'L':
                FIESTA_VERIFY(NextChar() == 'o', "'o' expected");
                FIESTA_VERIFY(NextChar() == 'g', "'g' expected");
                [[fallthrough]];
            case 'l':
                FIESTA_VERIFY(NextChar() == '[', "'[' expected");
                c = ParseAlgebraicSum();
                FIESTA_VERIFY(c == ']', "']' expected");
                AddOperation(Operation::LOG);
                nextPriority = Priority::L;
                term.nLog++;
                c = NextChar();
                break;
            case 'f':
            case 'x':
                FIESTA_VERIFY(NextChar() == '[', "'[' expected");
                n = 0;
                FIESTA_VERIFY(ParseInt(&n) == ']', "']' expected");
                /*Attention! The case x() will be treated as x[0]!*/
                if (c == 'x') {
                    operandStack.emplace(Operand(OperandType::x, n));
                    nextPriority = Priority::X;
                } else {
                    operandStack.emplace(Operand(OperandType::f, n));
                    nextPriority = Priority::F;
                }
                c = NextChar();
                break;
            case 'G': {
                Operand cpos = TestAddConstant("G", GetPredefinedConstant("EulerGamma"), true);
                nextPriority = Priority::C;
                operandStack.push(cpos);
                c = NextChar();
                break;
            }
#ifdef COMPLEX
            case 'I': {  // thats I, complex identity
                Operand cpos = TestAddConstant("I", "", false);
                nextPriority = Priority::C;
                operandStack.push(cpos);
                c = NextChar();
                break;
            }
#endif
            case 'P':
                /*Either P, or PolyGamma, or Power*/
                c = NextChar();
                if (c == 'o') {
                    c = NextChar();
                    if (c == 'w') {
                        FIESTA_VERIFY(NextChar() == 'e', "'er[' expected");
                        FIESTA_VERIFY(NextChar() == 'r', "'r[' expected");
                        [[fallthrough]];
                        case 'p':
                            FIESTA_VERIFY(NextChar() == '[', "'[' expected");
                            c = ParseAlgebraicSum();
                            FIESTA_VERIFY(c == ',', "',' expected");
                            Operand op = operandStack.top();
                            unsigned int xVar;
                            if (op.operandType == OperandType::x) {
                                xVar = op.operandNumber;
                            } else {
                                xVar = 0;
                            };
                            if ((xVar < 1) || (xVar > nx)) xVar = 0;
                            c = ParseExponent(xVar);
                            FIESTA_VERIFY(c == ']', "']' expected");
                            nextPriority = Priority::P;
                            term.nPow++;
                            c = NextChar();
                            break;
                    } /*if(c=='w')*/
                    else if (c == 'l') {
                        nextPriority = Priority::C;
                        c = ParsePolyGamma();
                        break;
                    }
                }      /*if(c=='o')*/
                else { /*pi, see a macros *_CONST_PI in the file constants.h*/
                    Operand cpos = TestAddConstant("Pi", GetPredefinedConstant("Pi"), true);
                    nextPriority = Priority::C;
                    operandStack.push(cpos);
                    break;
                } /*if(c=='o')...else*/
                FIESTA_FAIL("'P','Power' or 'PolyGamma' expected");
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                nextPriority = Priority::C;
                { /*Block*/
                    std::stringstream ss;
                    ss << c;
                    c = StringParseInt(ss);
                    if (c == '.') {
                        ss << '.';
                        c = StringParseInt(ss);
                    }
                    if (c == '*') {
                        char c2 = NextChar();
                        if (c2 == '^') {  // the way Mathamatica sends exponent
                            ss << 'E';
                            ss << NextChar();  // there should be at least one symbol after *^ and we have to use it to catch the -
                            c = StringParseInt(ss);
                        } else {  // no, that was just a multiplication
                            (theChar)--;
                        }
                    }
                    Operand cpos = TestAddConstant(ss.str(), "", true);

                    /*Replace division by multiplication:*/
                    if (nextOperation == Operation::DIV) {
                        nextOperation = Operation::MUL;
                        Operand cpos2(cpos.operandType, cpos.operandNumber + 1);
                        operandStack.push(cpos2);
                    } else {
                        operandStack.push(cpos);
                    }
                } /*Block*/
                break;
            default:
                FIESTA_FAIL(Format("Parse error: unexpected %c", c));
        } /*switch(c)*/

        FIESTA_VERIFY(nextPriority != Priority::Unknown, "ParseAlgebraicProduct logical error");
        diads.emplace_back(nextPriority, operandStack.top(), nextOperation);
        nextPriority = Priority::Unknown;
        operandStack.pop();
        dCounter++;
        /*The operation:*/
        switch (c) {
            case '*':
                nextOperation = Operation::MUL;
                term.nMul++;
                break;
            case '/':
                nextOperation = Operation::DIV;
                term.nDiv++;
                break;
            case ',':
            case '+':
            case '-':
            case ')':
            case ']':
            case ';':
                // here we finally put everything to the Operand stack
                std::sort(diads.begin(), diads.end());
                for (const auto &diad : diads) {
                    Operand operand;
                    Operation op;
                    std::tie(std::ignore, operand, op) = diad;
                    if (firstDiad) {
                        // if the first operation if division, we add 1 to the stack
                        if (op == Operation::DIV) {
                            operandStack.emplace(Operand(OperandType::constant, 1));
                        }
                    }
                    operandStack.push(operand);
                    if ((!firstDiad) || (op == Operation::DIV)) {
                        // if the first operation is multiplication, we no not add this operation
                        AddOperation(op);
                    }
                    firstDiad = false;
                }
                return c;
            default:
                FIESTA_FAIL(Format("Parse error: unexpected %c", c));
        } /*switch(c)*/
    }     /*for(;;)*/
}

char Scanner::ParseExponent(unsigned int xVar) {
    char c;
    unsigned int n;
    int neg = 0;
    do {
        c = NextChar();
        switch (c) {
            case '-':
                neg = -1;
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                n = c - '0'; /*first char is scanned already!*/
                c = ParseInt(&n);
                if (c == '.') {
                    unsigned int n2 = 0;
                    int e;
                    c = ParseInt(&n2, &e);
                    if (n2 != 0) { /* part after a . present*/
                        auto r = double(n) + double(n2) / e;
                        if (neg < 0) r *= -1.0;
                        std::stringstream expr;
                        if (neg < 0) {
                            expr << '-';
                        }
                        expr << n << '.' << n2;
                        operandStack.emplace(TestAddConstant(expr.str(), "", true));
                        if ((xVar > 0) && (r < 0)) {
                            if (maxX[xVar] == 0) maxX[0]++;
                            if (maxX[xVar] < ceil(-r)) maxX[xVar] = ceil(-r);
                        }
                        AddOperation(Operation::POW);
                        return c;
                    }
                }
                if (c == '/') {
                    unsigned int n2 = 0;
                    int e;
                    c = ParseInt(&n2, &e);
                    if (n2 != 0) { /*Fractional part present*/
                        auto r = double(n) / double(n2);
                        if (neg < 0) {
                            r *= -1.0;
                        }
                        std::stringstream expr;
                        if (neg < 0) {
                            expr << '-';
                        }
                        expr << n << '/' << n2;
                        operandStack.emplace(TestAddConstant(expr.str(), "", true));
                        if ((xVar > 0) && (r < 0)) {
                            if (maxX[xVar] == 0) maxX[0]++;
                            if (maxX[xVar] < ceil(-r)) {
                                maxX[xVar] = ceil(-r);
                            }
                        }
                        AddOperation(Operation::POW);
                        return c;
                    }
                }
                /*Integer power!*/
                if (n == 0) {
                    /*a^0=1, so pop one element and put 1:*/
                    operandStack.pop();
                    operandStack.emplace(Operand(OperandType::constant, 1));
                } else if (n > 1) {
                    switch (n) {
                        case 2:
                            AddOperation(Operation::IPOW2);
                            break;
                        case 3:
                            AddOperation(Operation::IPOW3);
                            break;
                        case 4:
                            AddOperation(Operation::IPOW4);
                            break;
                        case 5:
                            AddOperation(Operation::IPOW5);
                            break;
                        case 6:
                            AddOperation(Operation::IPOW6);
                            break;
                        default:
                            operandStack.emplace(Operand(OperandType::integer, n));
                            AddOperation(Operation::IPOW);
                    }
                }
                if (neg < 0) {
                    AddOperation(Operation::INV);
                    if (xVar > 0) {
                        if (maxX[xVar] == 0) {
                            maxX[0]++;
                        }
                        if (maxX[xVar] < n) {
                            maxX[xVar] = n;
                        }
                    }
                }
                return c;
        } /*switch(c)*/
    } while (neg < 0);
    // the cycle might happen only twice maximally in case we first scan '-', otherwise only once
    return c;
}

char Scanner::ParseAlgebraicSum() {
    std::vector<std::pair<Operation, TermInfo>> terms; // operation and Operand with information
    unsigned int termCounter = 0;
    Operation operation = Operation::PLUS;
    while (true) {
        TermInfo term;
        char c = ParseAlgebraicProduct(term, &operation);
        term.addr = operandStack.top();
        operandStack.pop();
        terms.emplace_back(operation, term);
        termCounter++;
        switch (c) {
            case '-':
                operation = Operation::MINUS;
                break;
            case '+':
                operation = Operation::PLUS;
                break;
            case ')':
            case ']':
            case ';':
            case ',':
                // sorting terms in order to have similar term close
                std::sort(terms.begin(), terms.end(), [] (const auto& t1, const auto& t2) {
                    const auto& [op1, info1] = t1;
                    const auto& [op2, info2] = t2;
                    if ((op1 == Operation::PLUS) && (op2 == Operation::MINUS)) return true;
                    if ((op1 == Operation::MINUS) && (op2 == Operation::PLUS)) return false;

                    return
                        std::tie(info1.addr, info1.nTriads, info1.nMul, info1.nDiv, info1.nPow, info1.nLog) <
                        std::tie(info2.addr, info2.nTriads, info2.nMul, info2.nDiv, info2.nPow, info2.nLog);
                });
                operandStack.push(terms[0].second.addr);
                if (terms[0].first == Operation::MINUS) {
                    AddOperation(Operation::NEG);
                }
                for (unsigned int i = 1; i < termCounter; i++) {
                    operandStack.push(terms[i].second.addr);
                    AddOperation(terms[i].first);
                }
                return c;
            default:
                FIESTA_FAIL(Format("Parse error: unexpected %c", c));
        }
    }
}

void Scanner::ParseIntegrand(const std::string& s) {
    char c;

    integrand = s;
    theChar = integrand.c_str();

    switch (c = NextChar()) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            nx = c - '0'; /*first char is scanned already!*/
            if (ParseInt(&nx) == ';') {
                break;
            }
            [[fallthrough]];
        default:
            FIESTA_FAIL(Format("Parse error: number of x with ; expected instead of %c", c));
    }
    nx++; /*x counted from 1, not 0!*/
    switch (c = NextChar()) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            nf = c - '0'; /*first char is scanned already!*/
            if (ParseInt(&nf) == ';') {
                break;
            }
            [[fallthrough]];
        default:
            FIESTA_FAIL(Format("Parse error: number of f with ; expected instead of %c", c));
    }
    nf++; /*f counted from 1, not 0!*/

    maxX = std::vector<unsigned int>(nx + 1);

    /*0 and 1 and I are pre-set:*/
    TestAddConstant("0", "", false);
    TestAddConstant("1", "", true);
#ifdef COMPLEX
    TestAddConstant("I", "", false);
#endif
    operandStack = std::stack<Operand>();
    f = std::vector<Operand>(nf);

    /*Scan all f first:*/
    for (unsigned int i = 1; i < nf; i++) {
        c = ParseAlgebraicSum();
        FIESTA_VERIFY(c == ';', "Parse error - ';' expected");
        /*Store result and remove it from the stack:*/
        f[i] = operandStack.top();
        operandStack.pop();
    }
    c = ParseAlgebraicSum();
    FIESTA_VERIFY(c == ';', "Parse error - ';' expected");

    if (operandStack.top().operandType != OperandType::Triad) {
        AddOperation(Operation::CPY);
    }

    /*Now convert ctTrian into rtTriad:*/
    triadHashTable.clear();

    // initializing the array of MP constants
    MPConstants.reserve(nativeConstants.size());
    for (unsigned int j = 0; j < nativeConstants.size(); j++) {
        MPConstants.emplace_back();
        InitMPVariable(MPConstants.data()+j);
    }
}
