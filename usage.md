# Basic usage instructions #

Most files are located in the FIESTA5 subfolder.

The main file FIESTA5.m should be loaded from Wolfram Mathematica, version 8+
However first you should build the binaries, at least KLink in order to let Mathematica write to databases.

Then you can run, for example,

SDEvaluate[UF[{k},{-k^2,-(k+p1)^2,-(k+p1+p2)^2,-(k+p1+p2+p4)^2},{p1^2->0,p2^2->0,p4^2->0,p1 p2->-S/2,p2 p4->-T/2,p1 p4->(S+T)/2,S->3,T->1}],{1,1,1,1},0]

Other binaries are not needed in case you set UsingC=False.

UsingQLink=True makes the program use the KLink binary to store files in the kyotocabinet database.

UsingC=True (default) makes the program use external C integration.
It uses the following sequence:

1) prepares database 1 with integrands
2) launches CIntegratePool
- CIntegratePool distributes task between threads running CIntegrateMP or CIntegrateMPC
- Mathematica communicates with CIntegratePool via file exchange and fills database 2 with results
3) runs GenerateAnswer[] in order to produce a result

If you run SDEvaluate (or any other similar command) with OnlyPrepare=True then Mathematica stops after stage 1 and prints out a command

Then you can perform stage 2 your self:
2) you launch CIntegratePool or CIntegratePoolMPI with the provided command
- the binary launched distributes task between threads running CIntegrateMP or CIntegrateMPC
- the results are saved by the binary in database 2
- the results are also printed out

In case the printed results are formatted not really well, you can repeat stage 3:

3) launch Mathematica, provide correct path to database 2 and run GenerateAnswer[] to produce your result

## Links ##

Other informative files in the package:

[CHANGELOG](changelog.md) - version information

[INSTALL](install.md) - install instructions on how to build the binaries

[DATABASE](database.md) - for internal database stucture

## Examples ##

The package comes with the following examples:

- examples/PolyGamma.int - sample integral input containing a non-evaluated PolyGamma. This example demonstrates how CIntegrateMP can call math in order to evaluate it
simply run bin/CIntegrateMP < examples/PolyGamma.int or examples/RunCIntegrateMP or examples/RunCIntegrate - the results should be identical
- examples/in.kch - sample database for tests (corresponding to SDEvaluateKU)
- examples/RunCIntegratePool (just run it from this folder) - it will run CIntegratePool and perform integration based on sample database examples/in.kch
- examples/RunCIntegratePoolMPI - same for MPI
- examples/examples.nb - mathematica notebook with examples

a number of examples to be loaded by math (for example, math < examples/SDEvaluate.m):

- examples/SDEvaluate.m - basic integration
- examples/SDEvaluateKU.m - shows Kaneko-Ueda strategy
- examples/SDEvaluateG.m - demonstrates Speer sectors approach
- examples/SDExpand.m - expanding
- examples/SDExpandNoC.m - expanding purely by Mathematica
- examples/SDExpandAsy.m - asymptotics with the regions approach; in order for it to work we added the asy code to the package (extra/asy2.2.m) however, to test it you would need to have qhull installed on your system. If you do not want to install it system-wide, edit the example and set QHullPath to point at QHull
- examples/SDExpandAsyRegVar.m - an example where one has to introduce additional regularization. This example also demonstrates how in some special cases Mathematica can integrate everything out analytically
- examples/SDEvaluateKU.m - example demonstrating strategy KU. This strategy also needs asy to be loaded and QHull to exist in the system
- examples/SDEvaluateDirect.m - integration with the direct syntax
- examples/SDExpandDirect.m - expansion with the direct syntax. Checks Polygammas and non-integer degrees
- examples/NoMathIntegration - run this file to see how Mathematica first prepares an integration database, then CIntegratePool is called to integrate, them Mathematica is again calls to produce results in a better format

The binary simlink bin/CIntegrateTool might be used to retrieve a certain integrand from a database. If the integration fails on a certain integral, it prints put a command with the use of OutputIntegrand. The output might be directed to a file and studied separately.

## Program options ##

- FIESTA5 is loaded in Mathematica. Run ?FIESTA to see the list of options
- CIntegratePool and CIntegrateTool accept commands from command line. Run any of them with --help to see the list of options
- CIntegrateMP has no options from command line apart from the list of files to replace stdin. You can run the binary and then enter Help to see the list of commands
