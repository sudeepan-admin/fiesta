# Database structure #

FIESTA.m prepares a key-value database for integration with the use of KLink (kyotocabinet).
The structure of the database is the following:

## Input database ##

key: 0-
value: list of tasks (for example, {1,2})
in standart SDEvaluate databases it will be usually equal to {1} (one task)
each task will be further denoted with {task}

key: 0-RegVar
value: name of the additional regularization variable; can be missing

key: 0-SmallVar
value: name of the additional variable tending to zero; only in SDExpandAsy

key: 0-RequestedOrders
value: pair of requested orders for ep and small var; only in SDExpandAsy

key: {task}-UsingC
value: True or False; a prepared database can be used only with the same UsingC setting

key: {task}-ExternalExponent
value: the exponent of small variable; everything is multiplied by t^value; it is 0 for basic examples

key: {task}-Exact
value: True or False; indicates whether we know an excat answer

in case of True for {task}-Exact we have only one more key for this {task}:

key: {task}-ExactValue
value: result for this {task}

otherwise we have the following keys:

key: {task}-ForEvaluation
value: list of pairs {ep order, {extvar order, extvar log order}} - coefficients that have to be evaluated

key: {task}-ForEvaluationString
value: same but elements are separated with | and ep order and extvar pair with -
example: 1-{1,1}|
the terms separated will be called {intdeg} below

key: {task}-SCounter
value: number of sectors; each {intdeg} will have SCounter integration terms (some are equal to zero)

key: {task}-{intdeg}-T
value: number of non-zero terms corresponding to {intdeg}

key: {task}-{intdeg}-T0
value: number of non-zero terms corresponding to {intdeg} in separate terms mode

key: {task}-{intdeg}-N
value: max number of variables in integrals with {intdeg}

key: {task}-{intdeg}-{num}
value: number of subterms in sector {num} for order {intdeg}

key: {task}-{intdeg}-{num}-{num2}
value: the integration term num2 in sector num for order {intdeg}
the term is ended with ;\n

key: {task}-{intdeg}-{num}-{num2}F
value: the F function of integration term num2 in sector num for order {intdeg}
the term is ended with ;\n

key: {task}-{intdeg}-{num}-{num2}-N
value: max number of variables in integrals with {intdeg} {num} and {num2}

key: {task}-runorder
value: maximal order to run integration till; coincides with the maximal value of first parts of {intdeg}

key: {task}-SHIFT
value: order shift between requested order and runorder; corresponds to the ep-order of external terms

key: {task}-EXTERNAL
value: external terms outside of integration

key: {task}-EXTERNAL-{deg}
value: numerical series coefficient of EXTERNAL of order {deg}

key: {task}-EXTERNAL-{deg}E
value: exact series coefficient of EXTERNAL of order {deg}

## Output database ##

Output database have the following copied entries:

0-, 0-RegVar, 0-SmallVar,

{task}-Exact

and depending on its value

{task}-ExactValue

or remaining keys

{task}-ForEvaluation, {task}-ExpandVariable, {task}-runorder, {task}-SHIFT, {task}-EXTERNAL, {task}-EXTERNAL-{order}
their values are just copied from the first database.

key: {task}-{prefix}-R
value: result for this task and prefix

key: {task}-{prefix}-E
value: False or True, indicates whether the result was calculated exactly. If it was perfomed by CIntegratePool, it is always False

